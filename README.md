# SASS: Supervisor of Autonomous Software Systems

SASS is a prototype of framework for the supervision of autonomous software systems.

SASS can be used to simulate the runtime revision of requirements of a software system by means of an iterative hill climbing approach, where different heuristics can be applied for the selection of a new system configuration (i.e., which requirements to activate in all the different possible contexts)

####Code structure
    SASS
        config.r            //configuration file
        loadSASS.r          //script to load and initialize SASS
        >src                //the source code, including the init file
            >framework      //utilities and heuristics for SASS
            >experimenter   //scripts to run to execute experiments with SASS
        >simulations        //folder containing the datasets to be used during the experiments.

####Quick Start

For a sample execution with the data obtained from [CrowdNavExt](https://bitbucket.org/dellannadavide/crowdnavext) and contained in the folder *simulations/crowdnavext* follow the following steps:

1. Modify files *config.r* and *loadSASS.r* with the correct PATHS for the used variables
2. Modify file *experimenter/experimenter.r* with the correct PATHS (and to define the desired experiments)
3. Run the *experimenter/experimenter.r* file.

####Use your own data
To use SASS with your own data, obtained from different simulations, follow the next steps:

1. Create a new folder in folder *simulations*, containing at least a folder with the datasets for all the system configurations to be tried.
2. Modify files *config.r* and *loadSASS.r* as described in step 1 of quick start section above.
3. If necessary set the parameter FAST_EXECUTION to TRUE
4. Define and run experiments as described in steps 2 and 3 of quick start section above.

####Contact
For any information or comment do not exitate to get in touch with me.

[Davide Dell'Anna](http://davidedellanna.com/)

Personal email: [dellannadavide@gmail.com](mailto:dellannadavide@gmail.com)

Institutional email: [d.dellanna@uu.nl](mailto:d.dellanna@uu.nl)

####Related work
- Dell'Anna, Davide, Fabiano Dalpiaz, and Mehdi Dastani. "Validating goal models via Bayesian networks." In 2018 5th International Workshop on Artificial Intelligence for Requirements Engineering (AIRE), pp. 39-46. IEEE, 2018.
- Dell’Anna, Davide, Mehdi Dastani, and Fabiano Dalpiaz. "Runtime Norm Revision Using Bayesian Networks." In International Conference on Principles and Practice of Multi-Agent Systems, pp. 279-295. Springer, Cham, 2018.

