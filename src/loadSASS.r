#The main folder containing everything
SASS_FOLDER <- file.path("C:", "Users", "DellA003", "surfdrive", "PhD", "PhD Project", "Stuff", "Softwares", "SASS")

# Load the config file
source(file.path(SASS_FOLDER, "src", "config.r"))

# Move to framework directory
setwd(file.path(SASS_FOLDER, "src", "framework"))

# Start SASS
source("init.r")
