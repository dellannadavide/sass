library(bnlearn)
set.seed(1165)
require(plotrix)

#The main folder containing everything
SASS_FOLDER <- file.path("C:", "Users", "DellA003", "surfdrive", "PhD", "PhD Project", "Stuff", "Softwares", "SASS")
# Move to framework directory
setwd(file.path(SASS_FOLDER, "src", "framework"))

source(file.path("utils", "utils.r"))
source(file.path("baseHillClimbing", "baseHillClimbing.r"))
source(file.path("BNstateBased", "BNstateBased.r"))
source(file.path("BNstateBased", "BNstateBasedEXP.r"))
source(file.path("BNstateBased", "BNstateBasedEXP_pure.r"))
source(file.path("hillClimbingMod", "hillClimbingMod.r"))
source(file.path("pureHillClimbing", "pureHillClimbing.r"))
source(file.path("pureHillClimbingMod", "pureHillClimbingMod.r"))

INFERENCE_PARAM <- 10000000

# The folder containing the material concerning the simulations.
# N.B. This folder will also be the WORKING DIRECTORY for SASS
SIMULATION_FOLDER <- file.path(SASS_FOLDER, "simulations", "crowdnavrtx")

# The folder containing the datasets to use to train the network during the execution of SASS_FOLDER
# Notice that all datasets should be named according to the following rule: "<DATASET_NAME_TYPE>_<n>" where DATASET_NAME_TYPE is below defined and n is an integer s.t. 1 <= n <= number of system configurations
DATASETS_FOLDER <-  file.path(SIMULATION_FOLDER, "dataset", "IF")

# Setting the working directory to the simulation folder
setwd(SIMULATION_FOLDER)

NET_MODEL = "[C_W][C_T][V_NPAP][N_NN|C_W:C_T][N_NPS|C_W:C_T][N_NPA|C_W:C_T:V_NPAP][N_NP|N_NPA:V_NPAP:N_NPS][N_N|N_NP:N_NN:C_W:C_T][O_N1|N_NPA:V_NPAP:N_NPS:C_W:C_T][O_N2|N_NPA:V_NPAP:N_NPS:C_W:C_T][O_N3|N_NPA:V_NPAP:N_NPS:C_W:C_T][N_JTA|C_W:C_T][N_JN|C_W:C_T][N_JTS|C_W:C_T][N_JTP|C_W:C_T][N_JT|N_JTA:N_JTS:N_JTP][N_J|N_JT:N_JN][O_1|N_N:N_J:C_W:C_T][O_2|N_J:C_W:C_T]"

# A dataset combining all the datasets for all the tried configurations
dataset_per_if = c("all.txt")

generateBN <- function(dataset, net_model) {
	net = model2network(net_model)
	req_dataset = read.table(dataset, header = TRUE)
	fit = bn.fit(net, req_dataset)
	temp_grain = as.grain(fit) #to normalize missing params
	fit = as.bn.fit(temp_grain) #to normalize missing params
	#write.dsc("netica.dsc", fit)
	fit
}

bn = generateBN(file.path(DATASETS_FOLDER, dataset_per_if[1]), NET_MODEL)

getGoalNodes <- function(bn) {
	nodes.all = nodes(bn)
	nodes.all[grepl("^O+", nodes.all)]
}
getStr <- function(n, val) {
	if(val=="disabled") paste("(", n, "==\"disabled\")",sep="")
	else if(val=="active") paste("(!(", n, "==\"disabled\"))",sep="")
	else if(val=="not_ob") paste("(!(", n, "==\"obeyed\"))",sep="")
	else paste("(", n, "==\"",val,"\")",sep="")
}
powerSetF = function(set){ ps = vector(mode="list",length=length(set)^2); ps[[1]] = numeric(); for(e in 1:length(set)){ f = 2^(e-1); for(subset in 1:f){ ps[[f+subset]] = c(ps[[subset]],set[e]); } } ; return(ps) }


context = paste(getStr("C_T", "night"),"&",getStr("C_W", "extreme"), sep="")

goals = paste(getStr(getGoalNodes(bn), "True"), collapse="&")
goals_expr <<- parse(text=goals)

# satisfiability assumptions
print("satisf ass")
# the list of requirements of which we want to evaluate the satisfiability assumption
obeyed_list = list(
				c("N_N"),
				c("N_NP"),
				c("N_NPA"),
				c("N_NPS"),
				c("N_NN"),
				c("N_J"),
				c("N_JN"),
				c("N_JT"),
				c("N_JTA"),
				c("N_JTP"),
				c("N_JTS")
			)
for(i in 1:length(obeyed_list)) {
	ol= obeyed_list[[i]]
	reqs = paste(getStr(ol, "obeyed"))
	reqs_expr <<- parse(text=reqs)
	evidence = paste(context)
	evidence_expr <<- parse(text=evidence)
	#print(evidence_expr)
	p1 = cpquery(bn, eval(reqs_expr), eval(evidence_expr), n=INFERENCE_PARAM)
	reqs = paste(getStr(ol, "violated"))
	reqs_expr <<- parse(text=reqs)
	#print(evidence_expr)
	p2 = cpquery(bn, eval(reqs_expr), eval(evidence_expr), n=INFERENCE_PARAM)
	diff = p1-p2
	print(diff)
}

# goals achievement
print("obj ach ass")
goal_list = getGoalNodes(bn)
for(i in 1:length(goal_list)) {
	g= goal_list[[i]]
	print(g)
	goal = paste(getStr(g, "True"))
	goal_expr <<- parse(text=goal)
	evidence = paste(context)
	evidence_expr <<- parse(text=evidence)
	#print(evidence_expr)
	p1 = cpquery(bn, eval(goal_expr), eval(evidence_expr), n=INFERENCE_PARAM)
	goal = paste(getStr(g, "False"))
	goal_expr <<- parse(text=goal)
	#print(evidence_expr)
	p2 = cpquery(bn, eval(goal_expr), eval(evidence_expr), n=INFERENCE_PARAM)
	diff = p1-p2
	print(diff)
}

# contribution
print("contrib ass")
goal_list = list(
				c("O_1"),
				c("O_1"),
				c("O_2"),
				c("O_N1"),
				c("O_N2"),
				c("O_N3")
			)
req_list = list(
				c("N_N"),
				c("N_J"),
				c("N_J"),
				c("N_NP"),
				c("N_NP"),
				c("N_NP")
			)
for(i in 1:length(goal_list)) {
	g= goal_list[[i]]
	r= req_list[[i]]
	goal = paste(getStr(g, "True"))
	goal_expr <<- parse(text=goal)
	reqs = paste(getStr(r, "obeyed"), collapse="&")
	evidence = paste(context, "&", reqs, sep="")
	evidence_expr <<- parse(text=evidence)
	#print(evidence_expr)
	p1 = cpquery(bn, eval(goal_expr), eval(evidence_expr), n=INFERENCE_PARAM)
	reqs = paste(getStr(r, "violated"), collapse="&")
	evidence = paste(context, "&", reqs, sep="")
	evidence_expr <<- parse(text=evidence)
	#print(evidence_expr)
	p2 = cpquery(bn, eval(goal_expr), eval(evidence_expr), n=INFERENCE_PARAM)
	diff = p1-p2
	print(diff)
}

# AND REFINEMENTS
print("and ref")
req_list = list(
				c("N_N"),
				c("N_J")
			)
req_list2 = list(
				c("N_NP", "N_NN"),
				c("N_JN", "N_JT")
			)
for(i in 1:length(req_list)) {
	r1= req_list[[i]]
	r2= req_list2[[i]]
	req = paste(getStr(r1, "obeyed"))
	req_expr <<- parse(text=req)
	reqs2 = paste(getStr(r2, "obeyed"), collapse="&")
	reqs2_expr <<- parse(text=reqs2)
	evidence = paste(context, "&", reqs2, sep="")
	evidence_expr <<- parse(text=evidence)
	#print(evidence_expr)
	# P(r1_ob | allSubReq_ob and context)
	p1 = cpquery(bn, eval(req_expr), eval(evidence_expr), n=INFERENCE_PARAM)
	#print("P(r1_ob | allSubReq_ob and context)")
	#print(p1)
	# P(r1_ob | context)
	evidence_expr <<- parse(text=context)
	pt = cpquery(bn, eval(req_expr), eval(evidence_expr), n=INFERENCE_PARAM)
	#print("P(r1_ob | context)")
	#print(pt)
	# P(allSubReq_ob | context)
	ps = cpquery(bn, eval(reqs2_expr), eval(evidence_expr), n=INFERENCE_PARAM)
	#print("P(allSubReq_ob | context)")
	#print(ps)
	#print("contrib allSubReq_ob to P(r1_ob)")
	#print((ps*p1))
	p2 = pt-(ps*p1)
	#print("P(r1_ob | notAllSubReq_obTogether and context)")
	#print(p2)
	diff = p1-p2
	#print("P(r1_ob | allSubReq_ob and context) - P(r1_ob | notAllSubReq_obTogether and context)")
	print(diff)
}

# (XOR) REFINEMENTS
print("xor ref")
req_list = list(
				c("N_NP"),
				c("N_JT")
			)
req_list2 = list(
				c("N_NPA", "N_NPS"),
				c("N_JTA", "N_JTP", "N_JTS")
			)
for(i in 1:length(req_list)) {
	r1= req_list[[i]]
	r2= req_list2[[i]]
	req = paste(getStr(r1, "obeyed"))
	req_expr <<- parse(text=req)
	evidence_expr <<- parse(text=context)
	pt = cpquery(bn, eval(req_expr), eval(evidence_expr), n=INFERENCE_PARAM)
	#print("P(r1_ob | context)")
	#print(pt)
	# calculate the probability to have r1 obeyed given exactly one of the children obeyed (XOR)
	p1 = 0
	for(k in 1:length(r2)) {
		conf_s = ""
		for(m in 1:length(r2)) {
			if(k==m) {
				conf_s = paste(conf_s, ifelse(isBlank(conf_s),""," & "), getStr(r2[[m]], "obeyed"),sep="")
			}
			else {
				conf_s = paste(conf_s, ifelse(isBlank(conf_s),""," & "), getStr(r2[[m]], "not_ob"),sep="")
			}
		}
		conf_expr <<- parse(text=conf_s)
		evidence = paste(context, "&", conf_s, sep="")
		evidence_expr <<- parse(text=evidence)
		#print(evidence_expr)
		pt1 = cpquery(bn, eval(req_expr), eval(evidence_expr), n=INFERENCE_PARAM)
		#print("P(r1_ob | only1ob and context)")
		#print(pt1)
		evidence_expr <<- parse(text=context)
		pt1p = cpquery(bn, eval(conf_expr), eval(evidence_expr), n=INFERENCE_PARAM)
		#print("P(thatOnly1ob | context)")
		#print(pt1p)
		#print("contrib thatOnly1ob to P(r1_ob)")
		#print((pt1p*pt1))
		p1 = p1+(pt1p*pt1)
	}
	#print("P(r1_ob | allOnlyOnes_ob and context)")
	#print(p1)
	p2 = pt-p1
	#print("P(r1_ob | notOnlyOnes_ob and context)")
	#print(p2)
	diff = p1-p2
	#print("P(r1_ob | allOnlyOnes_ob and context) - P(r1_ob | notOnlyOnes_ob and context)")
	print(diff)
}

# adoptability
print("adoptability ass")
req_list = list(
				c("N_N"),
				c("N_N"),
				c("N_NP"),
				c("N_NP"),
				c("N_J"),
				c("N_J"),
				c("N_JT"),
				c("N_JT"),
				c("N_JT")
			)
req_list2 = list(
				c("N_NP"),
				c("N_NN"),
				c("N_NPA"),
				c("N_NPS"),
				c("N_JN"),
				c("N_JT"),
				c("N_JTA"),
				c("N_JTP"),
				c("N_JTS")
			)
for(i in 1:length(req_list)) {
	r1= req_list[[i]]
	r2= req_list2[[i]]
	req = paste(getStr(r1, "obeyed"))
	req_expr <<- parse(text=req)
	req2 = paste(getStr(r2, "obeyed"))
	evidence = paste(context, "&", req2, sep="")
	evidence_expr <<- parse(text=evidence)
	#print(evidence_expr)
	p1 = cpquery(bn, eval(req_expr), eval(evidence_expr), n=INFERENCE_PARAM)
	#print(p1)
	req2 = paste(getStr(r2, "violated"))
	evidence = paste(context, "&", req2, sep="")
	evidence_expr <<- parse(text=evidence)
	#print(evidence_expr)
	p2 = cpquery(bn, eval(req_expr), eval(evidence_expr), n=INFERENCE_PARAM)
	#print(p2)
	diff = p1-p2
	print(diff)
}

# necessity assumptions
print("necessity ass")
active_list = list(
				c("N_N"),
				c("N_N", "N_NP"),
				c("N_N", "N_NP","N_NPA"),
				c("N_N", "N_NP","N_NPS"),
				c("N_N", "N_NN"),
				c("N_J"),
				c("N_J","N_JN"),
				c("N_J","N_JT"),
				c("N_J","N_JT","N_JTA"),
				c("N_J","N_JT","N_JTP"),
				c("N_J","N_JT","N_JTS")
			)
disable_list = list(
				c("N_N", "N_NP", "N_NN","N_NPA","N_NPS"),
				c("N_NP"),
				c("N_NPA"),
				c("N_NPS"),
				c("N_NN"),
				c("N_J","N_JN","N_JT","N_JTA","N_JTP","N_JTS"),
				c("N_JN"),
				c("N_JT"),
				c("N_JTA"),
				c("N_JTP"),
				c("N_JTS")
			)
for(i in 1:length(active_list)) {
	al= active_list[[i]]
	dl = disable_list[[i]]
	reqs = paste(getStr(al, "active"), collapse="&")
	evidence = paste(context, "&", reqs, sep="")
	evidence_expr <<- parse(text=evidence)
	#print(evidence_expr)
	p1 = cpquery(bn, eval(goals_expr), eval(evidence_expr), n=INFERENCE_PARAM)
	reqs = paste(getStr(dl, "disabled"), collapse="&")
	evidence = paste(context, "&", reqs, sep="")
	evidence_expr <<- parse(text=evidence)
	#print(evidence_expr)
	p2 = cpquery(bn, eval(goals_expr), eval(evidence_expr), n=INFERENCE_PARAM)
	diff = p1-p2
	print(diff)
}


# optionality
#print("7. optionality ass")
#disable_list = list(
#				c("N_N", "N_NP", "N_NN","N_NPA","N_NPS"),
#				c("N_JT","N_JTA","N_JTP","N_JTS")
#			)
#active_list = list(
#				c("N_N"),
#				c("N_JT")
#			)
#for(i in 1:length(active_list)) {
#	dl = disable_list[[i]]
#	al= active_list[[i]]
#	reqs = paste(getStr(dl, "disabled"), collapse="&")
#	evidence = paste(context, "&", reqs, sep="")
#	evidence_expr <<- parse(text=evidence)
#	#print(evidence_expr)
#	p1 = cpquery(bn, eval(goals_expr), eval(evidence_expr), n=INFERENCE_PARAM)
#	reqs = paste(getStr(al, "active"), collapse="&")
#	evidence = paste(context, "&", reqs, sep="")
#	evidence_expr <<- parse(text=evidence)
#	#print(evidence_expr)
#	p2 = cpquery(bn, eval(goals_expr), eval(evidence_expr), n=INFERENCE_PARAM)
#	diff = p1-p2
#	print(diff)
#}