########################## EXPERIMENTER ##########################

# Utility functions
writeOnFile <- function(algo, initsol, threshold, result) {
	write(c(algo, initsol, threshold, result[[1]], result[[2]], result[[3]], result[[4]]), file=RESULTS, append = TRUE, sep = "\t",ncolumns = 3+length(result[[1]])+length(result[[2]])+length(result[[3]])+length(result[[4]]))
}
writeOnFile2 <- function(algo, initsol, result) {
	write(c(algo, initsol, result[[1]], result[[2]], result[[3]], result[[4]], result[[5]]), file=RESULTS, append = TRUE, sep = "\t",ncolumns = 2+length(result[[1]])+length(result[[2]])+length(result[[3]])+length(result[[4]])+length(result[[5]]))
}

# Function that compares SASS with different heuristics starting from a specific initial system configuration with one specific threshold
compareAlgo <- function(initSol, threshold) {

	print(paste("Initial configuration: ", initSol))

	print("HILL CLIMBING MODIFICATO NEIGHBORHOOD WITH MAX LENGTH")
	res = pure_hillClimbingMod(initSol, threshold, FALSE, 1)
	writeOnFile("L10", initSol, threshold, res)

	print("HILL CLIMBING MODIFICATO NEIGHBORHOOD WITH MAX DIST")
	res = pure_hillClimbingMod(initSol, threshold, FALSE, 2)
	writeOnFile("D8", initSol, threshold, res)

	print("HILL CLIMBING MODIFICATO NEIGHBORHOOD WITH MAX LENGTH")
	res = pure_hillClimbingMod(initSol, threshold, FALSE, 3)
	writeOnFile("l20", initSol, threshold, res)

	print("STATE BASED USING BN AND ORIGINAL IDEA FOR SUGGESTION")
	res = BN_stateBasedEXP_pure(initSol, threshold, 1)
	writeOnFile("PB", initSol, threshold, res)

	print("STATE BASED USING BN AND A DIFFERENT IDEA FOR SUGGESTION")
	res = BN_stateBasedEXP_pure(initSol, threshold, 2)
	writeOnFile("SB", initSol, threshold, res)
}

# Function that compares SASS with different heuristics starting from ALL possible initial system configurations with one specific threshold
compareAlgoToT <- function(threshold) {
	steps = c(0,0,0,0,0,0,0,0,0,0)
	avg_sol_val = c(0,0,0,0,0,0,0,0,0,0)
	optimum = c(0,0,0,0,0,0,0,0,0,0)
	sequence = list(matrix(rep_len(list(), num_if), nrow = num_if, ncol = 1),
	matrix(rep_len(list(), num_if), nrow = num_if, ncol = 1),
	matrix(rep_len(list(), num_if), nrow = num_if, ncol = 1),
	matrix(rep_len(list(), num_if), nrow = num_if, ncol = 1),
	matrix(rep_len(list(), num_if), nrow = num_if, ncol = 1),
	matrix(rep_len(list(), num_if), nrow = num_if, ncol = 1),
	matrix(rep_len(list(), num_if), nrow = num_if, ncol = 1),
	matrix(rep_len(list(), num_if), nrow = num_if, ncol = 1),
	matrix(rep_len(list(), num_if), nrow = num_if, ncol = 1),
	matrix(rep_len(list(), num_if), nrow = num_if, ncol = 1))
	#cycle for all possible system configurations
	for(initSol in 1:num_if) {
		print(paste("ITERATION (INITSOL): ", initSol))

		res = pure_hillClimbing(initSol,threshold,FALSE, 1)
		steps[1] = steps[1] + res[[1]]
		avg_sol_val[1] = avg_sol_val[1] + res[[2]]
		optimum[1] = optimum[1] + res[[3]]
		sequence[[1]][[initSol]] = res[[4]]
		writeOnFile("HCPNML10", initSol, threshold, res)

		res = pure_hillClimbing(initSol,threshold,FALSE, 2)
		steps[2] = steps[2] + res[[1]]
		avg_sol_val[2] = avg_sol_val[2] + res[[2]]
		optimum[2] = optimum[2] + res[[3]]
		sequence[[2]][[initSol]] = res[[4]]
		writeOnFile("HCPNMD8", initSol, threshold, res)

		res = pure_hillClimbing(initSol,threshold,FALSE, 3)
		steps[3] = steps[3] + res[[1]]
		avg_sol_val[3] = avg_sol_val[3] + res[[2]]
		optimum[3] = optimum[3] + res[[3]]
		sequence[[3]][[initSol]] = res[[4]]
		writeOnFile("HCPNML20", initSol, threshold, res)

		res = pure_hillClimbing(initSol,threshold,TRUE, 1)
		steps[4] = steps[4] + res[[1]]
		avg_sol_val[4] = avg_sol_val[4] + res[[2]]
		optimum[4] = optimum[4] + res[[3]]
		sequence[[4]][[initSol]] = res[[4]]
		writeOnFile("HCPAN", initSol, threshold, res)

		res = pure_hillClimbingMod(initSol, threshold, TRUE)
		steps[5] = steps[5] + res[[1]]
		avg_sol_val[5] = avg_sol_val[5] + res[[2]]
		optimum[5] = optimum[5] + res[[3]]
		sequence[[5]][[initSol]] = res[[4]]
		writeOnFile("HCPMAN_NEW", initSol, threshold, res)

		res = pure_hillClimbingMod(initSol, threshold, FALSE, 1)
		steps[6] = steps[6] + res[[1]]
		avg_sol_val[6] = avg_sol_val[6] + res[[2]]
		optimum[6] = optimum[6] + res[[3]]
		sequence[[6]][[initSol]] = res[[4]]
		writeOnFile("HCPMNML10_NEW", initSol, threshold, res)

		res = pure_hillClimbingMod(initSol, threshold, FALSE, 2)
		steps[7] = steps[7] + res[[1]]
		avg_sol_val[7] = avg_sol_val[7] + res[[2]]
		optimum[7] = optimum[7] + res[[3]]
		sequence[[7]][[initSol]] = res[[4]]
		writeOnFile("HCPMNMD8_NEW", initSol, threshold, res)

		res = pure_hillClimbingMod(initSol, threshold, FALSE, 3)
		steps[8] = steps[8] + res[[1]]
		avg_sol_val[8] = avg_sol_val[8] + res[[2]]
		optimum[8] = optimum[8] + res[[3]]
		sequence[[8]][[initSol]] = res[[4]]
		writeOnFile("HCPMNML20_NEW", initSol, threshold, res)

		currDataset = ""
		res = BN_stateBasedEXP_pure(initSol, c(threshold), 1)
		steps[9] = steps[9] + res[[1]]
		avg_sol_val[9] = avg_sol_val[9] + res[[2]]
		optimum[9] = optimum[9] + res[[3]]
		sequence[[9]][[initSol]] = res[[4]]
		writeOnFile("PUREBN", initSol, threshold, res)

		currDataset = ""
		res = BN_stateBasedEXP_pure(initSol, c(threshold), 2)
		steps[10] = steps[10] + res[[1]]
		avg_sol_val[10] = avg_sol_val[5] + res[[2]]
		optimum[10] = optimum[5] + res[[3]]
		sequence[[10]][[initSol]] = res[[4]]
		writeOnFile("STATEBASED", initSol, threshold, res)
	}
	avg_sol_val = avg_sol_val/num_if
	steps = steps/num_if
	optimum = optimum/num_if
	print("Avg nr. steps:")
	print(steps)
	print("Avg value of final solution:")
	print(avg_sol_val)
	print("Percentage of times optimum found:")
	print(optimum)
}

# Function that compares SASS ONLY with the two informed heuristics (based on the Bayesian Network) from ALL possible initial system configurations, with a list of thresholds
# N.B. the list of thresholds must be ordered from small to big
compareBNEXPTot <- function(thresholds) {
	for(initSol in 1:num_if) {
		print(paste("ITERATION (INITSOL): ", initSol))
		currDataset = ""
		res = BN_stateBasedEXP_pure(initSol, thresholds, 1)
		for(r in res) {
			writeOnFile2("PUREBN", initSol, r)
		}
		currDataset = ""
		res = BN_stateBasedEXP_pure(initSol, thresholds, 2)
		for(r in res) {
			writeOnFile2("STATEBASED", initSol, r)
		}
	}
}

# Experiments parameters
good_req_sat = 0.8 #threshold for the good average requirement satisfaction. the same for all requirements
initSol = 1
neighborhood_max_length1 = 10
neighborhood_max_length2 = 20
neighborhood_max_dist = 8

#Load SASS
SASS_FOLDER <- file.path("C:", "Users", "DellA003", "surfdrive", "PhD", "PhD Project", "Stuff", "Softwares", "SASS")
source(file.path(SASS_FOLDER, "src", "loadSASS.r"))

#Start experiments
thresholds = c(0.25, 0.3, 0.35)
#thresholds = c(0.37, 0.4, 0.5)
now <- Sys.time()
RESULTS = paste0("ALL_", format(now, "%Y%m%d_%H%M%S_"), "RESULTS.txt")
compareAlgoToT(thresholds)


#Additional examples of experiments
#print("=========EXPERIMENT A: T 0.35===========")
#t=0.35
#print("=========EXPERIMENT A: T 0.5===========")
#t=0.50
#now <- Sys.time()
##RESULTS = paste0("T35_HCMOD_NEW", format(now, "%Y%m%d_%H%M%S_"), "RESULTS.txt")
#RESULTS = paste0("T50_SB_", format(now, "%Y%m%d_%H%M%S_"), "RESULTS.txt")
#compareAlgoToT(t)
##print("=========EXPERIMENT B: T 0.30===========")
##t=0.30
#print("=========EXPERIMENT B: T 0.40===========")
#t=0.40
#now <- Sys.time()
##RESULTS = paste0("T30_HCMOD_NEW", format(now, "%Y%m%d_%H%M%S_"), "RESULTS.txt")
#RESULTS = paste0("T40_SB_", format(now, "%Y%m%d_%H%M%S_"), "RESULTS.txt")
#compareAlgoToT(t)
##print("=========EXPERIMENT C: T 0.25===========")
##t=0.25
#print("=========EXPERIMENT C: T 0.37===========")
#t=0.37
#now <- Sys.time()
##RESULTS = paste0("T25_HCMOD_NEW", format(now, "%Y%m%d_%H%M%S_"), "RESULTS.txt")
#RESULTS = paste0("T37_SB_", format(now, "%Y%m%d_%H%M%S_"), "RESULTS.txt")
#compareAlgoToT(t)


