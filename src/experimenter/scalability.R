library(bnlearn)
set.seed(1165)
require(plotrix)

setwd("C:\\Users\\DellA003\\surfdrive\\PhD\\PhD Project\\Stuff\\Softwares\\SASS\\src\\framework")
source("utils\\utils.r")
source("BNstateBased\\BNstateBasedEXP_pure.r")
source("BNstateBased\\BN_utils.r")

FAST_EXECUTION = FALSE
SCALABILITY_EXPERIMENTATION = TRUE
good_req_sat = 0.8 #THRESHOLD, qui assumo che sia lo stesso per tutti quanti i REQ del sistema
algo_version = 1
t = 1
INFERENCE_PARAM <- 700000

setwd("C:\\Users\\DellA003\\surfdrive\\PhD\\PhD Project\\Stuff\\Softwares\\SASS\\simulations\\scalability")

DATASETS_FOLDER <- "DATASETS\\"

nets <- read.table("nets.txt", header = TRUE, sep = "\t", stringsAsFactors=FALSE)

for(i in 1:nrow(nets)) {
	id_net = nets[i,"ID"]
	NET_MODEL = nets[i, "NET"]
	dataset = paste(id_net, ".txt", sep="")
	nr_contexts = nets[i, "CONTEXT"]
	nr_contexts_vals = nets[i, "NR_C_VALS"]
	nr_reqs = nets[i, "REQ"]
	CONTEXTS <- c()
	g = expand.grid(rep(list(1:nr_contexts_vals),nr_contexts))
	for(r in 1:nrow(g)) {
		cont_str = ""
		for(c in 1:ncol(g)) {
			cont_str = paste(cont_str,"(C_",c,"==\"cv",g[r,c],"\")",sep="")
			if(c<ncol(g)) {
				cont_str = paste(cont_str,"&",sep="")
			}
		}
		contexts <- c(contexts, cont_str)
	}
	reqs = paste("N_", 1:nr_reqs, sep="")
	currBN = generateBN(file.path(DATASETS_FOLDER, dataset), net_model)
	#print(id_net)
	#print(net_model)
	#print(dataset)

	# print("Diagnosis...")
	matr =  matrix(0, nrow=length(contexts), ncol=length(getGoalNodes(currBN)))
	colnames(matr) = getGoalNodes(currBN)
	rownames(matr) = contexts
	goals = getGoalNodes(currBN)
	for(i in 1:length(contexts)) {
		for(j in 1:length(goals)) {
			matr[i,j] = getOverallSingleGoalSatisfInContext(currBN, contexts[i], reqs, c(), goals[j])
		}
	}
	r = rowMeans(matr)
	w = which(r == min(r), arr.ind = TRUE)

	for(context in contexts[w]) {
		problematic_goals = which(matr[context,] < t, arr.ind = TRUE)
		if(length(problematic_goals)<1) {
			probl_goals_names = getGoalNodes(currBN)[sample(1:length(goals), 1)] #QUI A DIFFERENZA DI BNSTATEBASE NORMALE ne scelgo uno a caso invece che
		}
		else {
			probl_goals_names = colnames(matr)[problematic_goals]
		}
		start.time <- Sys.time()
		suggConf = getSuggestedNewConfig(currBN, context, probl_goals_names, 0, reqs, c(), good_req_sat, t, algo_version, symbol)
		end.time <- Sys.time()
		time.taken <- end.time - start.time
		print(time.taken)
		break
	}

}
