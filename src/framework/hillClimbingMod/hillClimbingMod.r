########################## FRAMEWORK hillClimbing_mod ##########################

hillClimbing_mod <- function(initSol, t, all_neighbors, neighborhood_version) {
	# if(!FAST_EXECUTION) print("A step is taken only after considering the information acquired until now about the neighbors. It searches for a local optimum that is greather than the threshold defined by the user, if no configuration allows it then return the global optimum. If param all_neighbors is TRUE considers all the possible configurations as neighbors.")
	TRIED_IF <- c(initSol)
	ret_known <- init_ret_known
	stop=FALSE
	steps=0
	currSol = initSol
	ret_known[currSol] <- evalu(currSol)
	while(ret_known[currSol]<t && stop==FALSE) {
		L = getNeighborhood(currSol, all_neighbors, neighborhood_version)
		nextEval = -1
		nextSol = NULL
		distNext = 10000
		nextSolIsNew = TRUE
		if(nrow(L)>0) {
			for(r in 1:nrow(L)) {
				neighbor = L[[r,"index"]]
				if(ret_known[neighbor]> nextEval || isNew(neighbor, TRIED_IF)) {
					if(isNew(neighbor, TRIED_IF)) {
						if(nextSolIsNew && (L[[r,"dist"]] < distNext)) {
							nextSol = neighbor
							nextEval = ret_known[neighbor]
							distNext = L[[r,"dist"]]
							nextSolIsNew = TRUE
						}
					}
					else {
						if(ret_known[neighbor] > t) {
							nextSol = neighbor
							nextEval = ret_known[neighbor]
							nextSolIsNew = FALSE
						}
					}
				}
			}
		}
		if((nextEval <= ret_known[currSol]) && (is.null(nextSol) || !isNew(nextSol, TRIED_IF))) {
			stop=TRUE
			bestSol = currSol
			for(sol in TRIED_IF)
				if(ret_known[sol]>ret_known[bestSol])
					bestSol = sol
			# bestSol = which(ret_known==max(ret_known))[1]
			if(bestSol!=currSol) {
				steps = steps+1
				currSol = bestSol
				TRIED_IF <- c(TRIED_IF, currSol)
				# if(!FAST_EXECUTION) print(paste(currSol, ":", ret_known[currSol]))
			}
		}
		else {
			currSol = nextSol
			steps = steps+1
			if(isNew(currSol, TRIED_IF))
				TRIED_IF <- c(TRIED_IF, currSol)
			ret_known[currSol] <- evalu(currSol) #ottengo info sulla nuova sol
			# if(!FAST_EXECUTION) print(paste(currSol, ":", ret_known[currSol]))
		}
	}
	print(paste("Last configuration: ", currSol, "( steps: ", steps, ", val: ", ret_known[currSol], ")"))
	list(steps, ret_known[currSol], ifelse(ret_known[currSol]>=t,1,0),TRIED_IF)
}