########################## FRAMEWORK base_hillClimbing ##########################

#returns the number of steps and the currSol
base_hillClimbing <- function(initSol, t, neighborhood_version) {
	# if(!FAST_EXECUTION) print("A step is taken only after considering the information acquired until now about the neighbors. It is easy to end up in BAD local optima if the solution found is slighltly bettere than 0.5. N.B. it doesn't take into account the threshold. It is just a basic baseline.")
	TRIED_IF <- c()
	ret_known <- init_ret_known
	stop=FALSE
	steps=0
	currSol = initSol
	TRIED_IF <- c(TRIED_IF, currSol)
	ret_known[currSol] <- evalu(currSol)
	while(!stop) {
		L = getNeighborhood(currSol, FALSE, neighborhood_version)
		nextEval = -1
		nextSol = NULL
		if(is.null(nrow(L))) {
			n_list = L[["index"]]
		}
		else {
			n_list = L[,"index"]
		}
		for(neighbor in n_list) { #i vicini sono valutati solo in termini of information acquired e non c'è una vera esecuzione prima di valutarli tutti
			if(ret_known[neighbor] > nextEval) {
				nextSol = neighbor
				nextEval = ret_known[neighbor]
			}
		}
		if(nextEval <= ret_known[currSol]) {
			stop=TRUE
		}
		else {
			currSol = nextSol
			TRIED_IF <- c(TRIED_IF, currSol)
			steps = steps+1 #step è fatto solo quando si sceglie la next sol
			ret_known[currSol] <- evalu(currSol) #ottengo info sulla nuova sol
			# if(!FAST_EXECUTION)
			# 	print(paste(currSol, ":", ret_known[currSol]))
		}
	}
	list(steps, ret_known[currSol], ifelse(ret_known[currSol]>=t,1,0), TRIED_IF)
}


