source("BNstateBased\\BN_utils.r")

########################## FRAMEWORK BN_stateBased ##########################

BN_stateBasedEXP <- function(initIf, thresholds, algo_version) {
	results = list()

	ret_known <- init_ret_known #initialization
	#tried_if <- c(initIf)
	TRIED_IF <- c(initIf)
	updateTriedConfPerContext(initIf)
	steps=0
	currSol = initIf
	currBN = NULL
	nextSol = currSol

	currDataset <- generateNewDataset(DATASETS_FOLDER, currDataset, currSol)
	currBN = generateBN(file.path(DATASETS_FOLDER, currDataset), NET_MODEL)


	#AVERAGE OF RETURNS, FOR EACH CONTEXT
	# NOTO CHE DEVO FARE COSI INVECE CHE: ret_known[currSol] = getOverallGoalSatisf(currBN)
	# PERCHE se noi non diamo nessuna evidenza quando vogliamo sapere il  valore dei goal otterremo dei valori pesati rispetto a tutta l'esperienza passata, che normalmente non è quello che ci interessa.
	# currSol è l'IF che ha potenzialmente una configurazione diversa per ogni context config.
	#DAVIDE DOVRESTI ATTIVARE QUESTO
	if(FAST_EXECUTION) {
		ret_known[currSol] = evalu(currSol)
	}
	else {
		ret_known[currSol] = 0
		for(context in CONTEXTS) {
			ret_known[currSol] = ret_known[currSol] + exec(currBN, context, getActiveNormsPerConf(currSol, context, active_nodes_per_conf), getDisabledNormsPerConf(currSol, context, active_nodes_per_conf))
		}
		ret_known[currSol] = ret_known[currSol]/length(CONTEXTS)
	}

	for(t in thresholds) {
		print(paste("=========SET THRESHOLD AT",t,"==========="))
		if(!FAST_EXECUTION) {
			plot.new()
			plot.window(xlim=c(0,1), ylim=c(0,1))
			abline(a=6, b=3)
			axis(1)
			axis(2)
			title(xlab="Goal achievement")
			title(ylab="Norm Satisfaction")
			box()
			lines(x=c(0, 1), y=c(good_req_sat, good_req_sat), lwd=1, col="red")
			lines(x=c(t, t), y=c(0, 1), lwd=1, col="red")
			symbol = 1
		}


		# context= paste(getStr("C_Hour", "rush"))
		# questo qua sopra non va piu dato cosi ma va determinato dalla rete

		# print(paste("Return of ",currSol, ":", ret_known[currSol]))

		while(ret_known[currSol]<t && length(TRIED_IF)<num_if) {
			print("Diagnosis...")
			print(paste("Return of ",currSol,": ", ret_known[currSol]))
			matr =  matrix(0, nrow=length(CONTEXTS), ncol=length(getGoalNodes(currBN)))
			colnames(matr) = getGoalNodes(currBN)
			rownames(matr) = CONTEXTS
			goals = getGoalNodes(currBN)
			if(FAST_EXECUTION) {
				for(i in 1:length(CONTEXTS)) {
					for(j in 1:length(goals)) {
						matr[i,j] = returnsMatrix[currSol,paste0("C",i,goals[j])]
					}
				}
			}
			else {
				for(i in 1:length(CONTEXTS)) {
					for(j in 1:length(goals)) {
						matr[i,j] = getOverallSingleGoalSatisfInContext(currBN, CONTEXTS[i], getActiveNormsPerConf(currSol, CONTEXTS[i], active_nodes_per_conf), getDisabledNormsPerConf(currSol, CONTEXTS[i], active_nodes_per_conf), goals[j])
					}
				}
			}
			# if(!FAST_EXECUTION) print(matr)
			#PLOTTING
			# if(!FAST_EXECUTION) {
			# 	val_conf_goals= c()
			# 	for(i in 1:length(contexts)) {
			# 		curr_conf = getCurrConf(currBN,contexts[i],getActiveNormsPerConf(currSol, contexts[i], active_nodes_per_conf), getDisabledNormsPerConf(currSol, contexts[i], active_nodes_per_conf))
			# 		conf = paste(curr_conf, ifelse(isBlank(curr_conf),"","&"), contexts[i],sep="")
			# 		conf_expr <<- parse(text=conf)
			# 		goals = paste(getStr(getGoalNodes(currBN), "True"), collapse="&")
			# 		goals_expr <<- parse(text=goals)
			# 		val_conf_goals = c(val_conf_goals, cpquery(currBN, eval(goals_expr), eval(conf_expr), n=inference_param))
			# 		prob_list = c()
			# 		for(n in getActiveNormsPerConf(currSol, contexts[i], active_nodes_per_conf)) {
			# 			node_values = getNodeValues(currBN, n)
			# 			if("obeyed" %in% node_values) {
			# 				# # if(!FAST_EXECUTION) print(n)
			# 				e <<- parse(text=getStr(n, "obeyed"))
			# 				val = cpquery(currBN, eval(e), eval(conf_expr), n=inference_param)
			# 				prob_list = c(prob_list, val)
			# 				# # if(!FAST_EXECUTION) print(paste(n, val))
			# 			}
			# 		}
			# 	}
			# 	if(length(prob_list)==0)
			# 	prob = 1
			# 	else
			# 	prob = mean(prob_list)
			#
			# 	plotCI(mean(val_conf_goals), prob, ui=max(prob_list), li=min(prob_list),add=TRUE, pch=symbol, col=symbol+1)
			# 	plotCI(mean(val_conf_goals), prob, ui=max(val_conf_goals), li=min(val_conf_goals), err="x", add=TRUE, pch=symbol, col=symbol+1)
			# }

			r = rowMeans(matr[,])
			w = which(r == min(r), arr.ind = TRUE)

			#DETERMINE the MLE for the problem AND SHOW IT
			# if(!FAST_EXECUTION) {
			# 	print(paste("Most problematic contexts: ", contexts[w]))
			# 	print("for each of them perform diagnosis...")
			# 	for(context in contexts[w]) {
			# 		# if(!FAST_EXECUTION) print(paste("context: ", context))
			# 		# if(!FAST_EXECUTION) print(matr[context,])
			# 		problematic_goals = which(matr[context,] < t, arr.ind = TRUE)
			# 		if(length(problematic_goals)<1) {
			# 			probl_goals_names = getGoalNodes(currBN)[sample(1:length(goals), 1)] #QUI A DIFFERENZA DI BNSTATEBASE NORMALE ne scelgo uno a caso invece che
			# 		}
			# 		else {
			# 			probl_goals_names = names(problematic_goals)
			# 		}
			# 		# if(!FAST_EXECUTION) print("problematic_goals: ")
			# 		# if(!FAST_EXECUTION) print(names(problematic_goals))
			# 		goals = paste(getStr(probl_goals_names, "False"), collapse="&")
			# 		# if(!FAST_EXECUTION) print(paste("looking for an explanation for: ", goals))
			# 		mle_matrix = t(getMLE(currBN, goals, context, getActiveNormsPerConf(currSol, context, active_nodes_per_conf), getDisabledNormsPerConf(currSol, context, active_nodes_per_conf)))
			# 		print(mle_matrix)
			# 	}
			# }

			print("Suggestion...")
			# RECURSIVE PROCEDURE OR NORMS ALL SAME LEVEL
			# if(!FAST_EXECUTION) print("for each of the problematic contexts suggest revision...")
			L = list()
			for(context in CONTEXTS[w]) {
				print("...")
				# if(!FAST_EXECUTION) print(paste("context: ", context))
				#suggConf here is a list of suggested configurations for the context context
				# if(!FAST_EXECUTION) print("look for suggested new configurations...")
				problematic_goals = which(matr[context,] < t, arr.ind = TRUE)
				if(length(problematic_goals)<1) {
					probl_goals_names = getGoalNodes(currBN)[sample(1:length(goals), 1)] #QUI A DIFFERENZA DI BNSTATEBASE NORMALE ne scelgo uno a caso invece che
				}
				else {
					probl_goals_names = names(problematic_goals)
				}
				suggConf = getSuggestedNewConfig(currBN, context, probl_goals_names, currSol, getActiveNormsPerConf(currSol, context, active_nodes_per_conf), getDisabledNormsPerConf(currSol, context, active_nodes_per_conf), good_req_sat, t, algo_version, symbol)
				# if(!FAST_EXECUTION) print(paste("SUGGESTED CONFIGURATIONS for context:", context))
				# if(!FAST_EXECUTION) print(suggConf)
				if(length(suggConf)>0) {
					L[[context]] = c(suggConf)
				}
				else {
					#se nessun suggerimento è stato dato lascio quello corrente
					L[[context]] = c(ALL_IF[currSol,getContextId(context)])
				}
			}
			print("Revision...")
			#NOTO CHE QUA ORA ESPANDO TUTTO PER OTTENERE TUTTI I NUOVI IF CHE SODDISFANO I NUOVI SUGGERIMENTI LASCIANDO IL RESTO COSI COM'è,
			#MA UNA ALTERNATIVA E' ANCHE SCEGLIERNE UNO SOLO TODO
			new_conf_for_contexts = expand.grid(L) #genero tutte le combinazioni delle conf sugg per ogni contesto. qui genero if praticamente. POTREBBERO NON ESSERE SUPPORTATI PERO
			neighborhood = list()
			if(nrow(new_conf_for_contexts)>0) {
				# if(!FAST_EXECUTION) print(paste("new possible conf per contexts: ",new_conf_for_contexts))
				n = 1
				for(co in 1:nrow(new_conf_for_contexts)) {
					new_neighbor = c()
					for(i in 1:length(CONTEXTS)) {
						if(CONTEXTS[i] %in% CONTEXTS[w]) { #if the context is one of the problematics
							new_neighbor = c(new_neighbor, new_conf_for_contexts[co,CONTEXTS[i]])
						}
						else { #METTO QUELLO CORRENTE, ALL_IF[currSol,i]
							new_neighbor = c(new_neighbor, ALL_IF[currSol,i])
						}
					}
					# print(new_neighbor)
					if(getIFID(new_neighbor)>0) { #LO CONSIDERO SOLO SE ESISTE
						neighborhood[[n]] = new_neighbor
						n=n+1
					}
				}
			}

			print("...")
			if(length(neighborhood)>0) {
				# if(!FAST_EXECUTION) print("neighborhood")
				# if(!FAST_EXECUTION) print(neighborhood)
				sorted_neighborhood = sortNeighborhood(neighborhood, currSol)
				print("...")
				# if(!FAST_EXECUTION) print("sorted neighborhood")
				# if(!FAST_EXECUTION) print(sorted_neighborhood)

				if(is.null(nrow(sorted_neighborhood))) { #there is only one element in the neighborhood
					neighbor = neighborhood[[sorted_neighborhood["index"]]]
					neighbor_id = getIFID(neighbor)
					if(neighbor_id>0) { #in case the id is 0 it means that no IF has been found, IN TEORIA MAI QUI
						# if(!FAST_EXECUTION) print(paste("unique neighbor",neighbor_id))
						distance = distance_matrix[currSol, neighbor_id]
						#TODO FORSE BISOGNEREBBE CONTROLLARE ANCHE IS NEW PER UN CERTO CONTESTO SPECIFICO E NON SOLO SUGLI IF
						#TODO trasformare in funzione
						if(isNew(neighbor_id, TRIED_IF)) {
							## if(!FAST_EXECUTION) print("is new!")
							nextSol = neighbor_id
							steps = steps+1
						}
						else {
							if(!FAST_EXECUTION) print("not new...")
						}
					}
					print("...")
				}
				else {
					for(r in 1:nrow(sorted_neighborhood)) {
						neighbor = neighborhood[[sorted_neighborhood[r,"index"]]]
						neighbor_id = getIFID(neighbor)
						# if(!FAST_EXECUTION) print(neighbor_id)
						if(neighbor_id>0) { #in case the id is 0 i ignore this one
							distance = distance_matrix[currSol, neighbor_id]
							#TODO FORSE BISOGNEREBBE CONTROLLARE ANCHE IS NEW PER UN CERTO CONTESTO SPECIFICO E NON SOLO SUGLI IF
							#TODO trasformare in funzione
							if(isNew(neighbor_id, TRIED_IF)) {
								## if(!FAST_EXECUTION) print("is new!")
								nextSol = neighbor_id
								steps = steps+1
								break
								#TODO NOTO CHE QUA CON STO BREAK NON FACCIO IN REALTA QUELLO CHE FA HILL CLIMBING, PERCHé QUA MI FERMO AL PRIMO, E POI RICOMINCIO TUTTO IL LOOP, CIOè STO FACENDO UNA RICERCA IN PROFONDITA MENTRE HILL CLIMBING LA FA IN AMPIEZZA
								#PER ESTENDERLO SI POTREBBE, FARE LA EVALU DI NEIGHBOR_ID, ANCHE GENERANDO IL NUOVO DATASET E LA NUOVA BN IN MODO DA NON PERDERE L'ESECUZIONE (PERCHé SAREBBERO EFFETTIVAMENTE DEGLI STEPS)
								#SI INTRODUCE ANCHE LA LASTnEIG COME IN PUREHILLCLIMB E SI CONTROLLA SOTTO SE è DIVERSA. COSI DIVENTA HILL CLIMBING
							}
							else {
								if(!FAST_EXECUTION) print("not new...")
							}
						}
					}
				}
			}
			print("...")
			# if(!FAST_EXECUTION) print(paste("nextSol: ", nextSol))
			## if(!FAST_EXECUTION) print(currSol)
			if(nextSol==currSol) {
				# if(!FAST_EXECUTION) print("looking for a new one...")
				new_ifs = getNewIF(TRIED_IF)
				if(length(new_ifs) > 0) {
					min = new_ifs[1]
					for(i in new_ifs) {
						if(distance_matrix[currSol, i] < distance_matrix[currSol, min])
						min = i
					}
					nextSol = min
					steps = steps+1
				}
			}
			currSol = nextSol
			currDataset <- generateNewDataset(DATASETS_FOLDER, currDataset, currSol)
			currBN = generateBN(file.path(DATASETS_FOLDER, currDataset), NET_MODEL)
			# currBN = generateBN(currSol)
			#DAVIDE DOVRESTI ATTIVARE QUESTO
			if(FAST_EXECUTION) {
				ret_known[currSol] = evalu(currSol)
			}
			else {
				ret_known[currSol] = 0
				for(context in CONTEXTS) {
					ret_known[currSol] = ret_known[currSol] + exec(currBN, context, getActiveNormsPerConf(currSol, context, active_nodes_per_conf), getDisabledNormsPerConf(currSol, context, active_nodes_per_conf))
				}
				ret_known[currSol] = ret_known[currSol]/length(CONTEXTS)
			}

			TRIED_IF <- c(TRIED_IF, currSol)
			updateTriedConfPerContext(currSol)
			# if(!FAST_EXECUTION) print(paste(currSol, ":", ret_known[currSol]))
		}
		if(length(TRIED_IF)>=num_if) {
			bestSol = currSol
			for(sol in TRIED_IF)
			if(ret_known[sol]>ret_known[bestSol])
			bestSol = sol
			#bestSol = which(ret_known==max(ret_known))
			if(bestSol!=currSol) {
				steps = steps+1
				currSol=bestSol
				currDataset <- generateNewDataset(DATASETS_FOLDER, currDataset, currSol)
				currBN = generateBN(file.path(DATASETS_FOLDER, currDataset), NET_MODEL)
				# if(!FAST_EXECUTION) print(paste(currSol, ":", ret_known[currSol]))
			}
		}
		# if(!FAST_EXECUTION) print(paste("Last configuration: ", currSol, "( steps: ", steps, ", val: ", ret_known[currSol], ")"))
		print(steps)
		results[[paste0("T",t)]] = list(t, steps, ret_known[currSol], ifelse(ret_known[currSol]>=t,1,0),TRIED_IF)
	}
	results
}