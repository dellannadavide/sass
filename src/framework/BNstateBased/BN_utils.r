#curr_active_nodes <- c()
# active_nodes_per_conf <- matrix()
# good_req_satisfaction <- 0.5
# good_goal_achievement <- 0.5


#' reviseIF is the function called to determine whether a revision of the current requirements model is necessary and to obtain a suggestion of a revision of the model.
#' it first determines the probability of achievement of the goal in the current configuration (querying the BN)
#' if this probability is smaller than a defined threshold a revision of the current configuration is triggered
#' 
#' @param bn is a bn.fit object representing the current Bayesian Network with 
#'	all the information collected until now (see function generateBN to obtain such an object)
#' @param c is a string representing the context to analyze. More specifically it is the evidence to give to the network for 
#'	the contextual variables (not norms or goals variables)
#'	the param must have the form (c_i==val_i), ... ,(c_n==val_n), with c_i...c_n names of contextual nodes in the BN and val_i...val_n possible values for those nodes (see also getStr function)
#' @param conf_id is the id of the current configuration of requirements, it is used to determine the neighborhood
#' @param active_norms is a vector of string of names of the nodes in the BN representing the active norms in all contexts
#' @param good_req_sat is a numeric value representing the minimum probability (threshold) of satisfaction of the requirements needed before to be able to evaluate its efficacy.
#' @param good_goal_ach is a numeric value representing the minimum accepted probability (threshold) of achievement of the overall goal, if the actual probability is below good_goal_ach a revision is triggered
#' @param f is an identifier for the subfunction to use in case a revision is triggered. val 1: reviseIF_FirstIdea, val 2: reviseIF_Revised
#' @param plot_symbol is a vector of numbers defining the symbols to use in the plot
#'
#' @return a vector of possible suggested revised configuration ids
#' 
#' @examples
#' reviseIF(generateBN(1), paste(getStr("C_Hour", "rush")), 1, c("N_1"), 0.9, 0.6, 2, 1)
#' note that the above example can be executed only if there is a configuration with id 1 with at least one norm N_1 and a contextual var C_Hour that can take val rush
#' 
reviseIF <- function(bn, context, problematic_goals, if_id, curr_act, curr_dis, good_req_sat, good_goal_ach, f, plot_symbol) {
	# setCurrActiveNodes(active_norms)
	# active_nodes_per_conf = active_norms
	setGoodReqSatisfaction(good_req_sat)
	setGoodGoalAchievement(good_goal_ach)

	# curr_act = getCurrActiveNodes(bn, context)
	# curr_dis = getCurrDisabledNodes(bn, context)
	if(FAST_EXECUTION) {
		val_conf_goals = returnsMatrix[if_id,paste0("C",getContextId(context),"_GLOBAL")]
	}
	else {
		val_conf_goals = getOverallGoalSatisfInContext(bn, context, curr_act, curr_dis)
	}
	# prob_list = c()
	# for(n in curr_act) {
	# 	node_values = getNodeValues(bn, n)
	# 	if("obeyed" %in% node_values) {
	# 		# # if(!FAST_EXECUTION) print(n)
	# 		e <<- parse(text=getStr(n, "obeyed"))
	# 		val = cpquery(bn, eval(e), eval(conf_expr), n=inference_param)
	# 		prob_list = c(prob_list, val)
	# 		# # if(!FAST_EXECUTION) print(paste(n, val))
	# 	}
	# }
	# if(length(prob_list)==0)
	# 	prob = 1
	# else
	# 	prob = mean(prob_list)
	#plotCI(val_conf_goals, prob, ui=max(prob_list), li=min(prob_list),add=TRUE, pch=plot_symbol, col=plot_symbol+1)
	
	## if(!FAST_EXECUTION) print(paste("In context", context, ":"))
	# if(!FAST_EXECUTION) print(paste("Goal achievement: ", val_conf_goals))
	if(val_conf_goals>=good_goal_ach) {
		print("Goals achievement is already above the specified threshold");
		c()
	}
	else {
		if(f==1) {
			reviseIF_FirstIdea(bn, context,problematic_goals, if_id, curr_act, curr_dis)
		}
		else {
			reviseIF_Revised(bn,context,problematic_goals, if_id, curr_act, curr_dis, val_conf_goals)
		}
	}
}

getNodeIndex <- function(bn, node_string) {
	i = 0
	index = 0
	for(n in nodes(bn)) {
		i = i+1
		if(n==node_string)
			index = i
	}
	index
}

getNodeValues  <- function(bn, node_string) {
	node_index = getNodeIndex(bn, node_string)
	node_val_names = dimnames(bn[[node_index]]$prob)[1]
	unlist(node_val_names)
}

#' reviseIF_FirstIdea is the first idea of process to determine a suggestion of the requirements. See document \url{C:\Users\DellA003\surfdrive\PhD\PhD Project\Notes\Document_Norms_Revision\norms_revision.pdf} for more details
#' 
#' @param bn is a bn.fit object representing the current Bayesian Network with 
#'	all the information collected until now (see function generateBN to obtain such an object)
#' @param c is a string representing the context to analyze. More specifically it is the evidence to give to the network for 
#'	the contextual variables (not norms or goals variables)
#'	the param must have the form (c_i==val_i), ... ,(c_n==val_n), with c_i...c_n names of contextual nodes in the BN and val_i...val_n possible values for those nodes (see also getStr function)
#' @param conf_id is the id of the current configuration of requirements, it is used to determine the neighborhood
#' 
#' @return a vector of possible suggested revised configuration ids
#' 
#' @examples
#' reviseIF_FirstIdea(generateBN(1), paste(getStr("C_Hour", "rush")), 1)
#' note that the above example can be executed only if there is a configuration with id 1 with at least a contextual var C_Hour that can take val rush
#' 
reviseIF_FirstIdea <- function(bn, context, problematic_goals, if_id, curr_act, curr_dis) {
	n_new = getBestConf(bn, context, if_id, curr_act, curr_dis)
	n_a_new = n_new
	# if(!FAST_EXECUTION) print(paste("n_new: ", n_a_new))
	if(!SCALABILITY_EXPERIMENTATION) {
		n_new = getConfId(if_id, context, n_a_new)
	}
	if(n_new==0) {
		n_new = c()
	}
	if(length(curr_act)>0 && !allConfTried(context)) {
		n_e = getMLE(bn, paste(getStr(problematic_goals, "False"), collapse="&"), context,curr_act, curr_dis) #diverso da sol iniziale perché solo goal problematici
		# if(!FAST_EXECUTION) print(paste("n_e: ", colnames(n_e), n_e))
		for(n in n_a_new) {
			n_des = getDesiredVal(bn, n, context,problematic_goals, curr_act, curr_dis)
			# if(!FAST_EXECUTION) print(paste("n_des (",n,"):", n_des))
			sr = suggestRevision(n, n_e, n_des, context)
			if(!SCALABILITY_EXPERIMENTATION) {
				for(s in sr) {
					n_new = union(n_new, getRevisions(n, s, if_id, context)) #non è esattamente quello pensato, ma rende il tuto piu generico. ora ritorna l'unione di tutte le possibili configurazioni in cui le norme sono riviste come suggerito per ognuna di loro
				}
			}
		}
	}
	# if(!FAST_EXECUTION) print("new confs: ")
	# if(!FAST_EXECUTION) print(n_new)
	n_new
}

#' reviseIF_Revised is a new idea of process to determine a suggestion of the requirements based on the position of the current configuration on a plot goal_achiev/requirements_satisf. 
#' See document \url{C:\Users\DellA003\surfdrive\PhD\PhD Project\Notes\Notes\notes.pdf} for more details
#' 
#' @param bn is a bn.fit object representing the current Bayesian Network with 
#'	all the information collected until now (see function generateBN to obtain such an object)
#' @param c is a string representing the context to analyze. More specifically it is the evidence to give to the network for 
#'	the contextual variables (not norms or goals variables)
#'	the param must have the form (c_i==val_i), ... ,(c_n==val_n), with c_i...c_n names of contextual nodes in the BN and val_i...val_n possible values for those nodes (see also getStr function)
#' @param conf_id is the id of the current configuration of requirements, it is used to determine the neighborhood
#' 
#' @return a vector of possible suggested revised configuration ids
#' 
#' @examples
#' reviseIF_Revised(generateBN(1), paste(getStr("C_Hour", "rush")), 1)
#' note that the above example can be executed only if there is a configuration with id 1 with at least a contextual var C_Hour that can take val rush
#' 
reviseIF_Revised <- function(bn, c,problematic_goals, if_id, curr_act, curr_dis, val_conf_goals) {
	if(FAST_EXECUTION) {
		prob = returnsMatrix[if_id, paste0("C",getContextId(c),"_PROB")]
	}
	else {
		curr_conf = getCurrConf(bn,c, curr_act, curr_dis)
		# # if(!FAST_EXECUTION) print("curr_conf: ")
		# # if(!FAST_EXECUTION) print(curr_act)
		conf = paste(curr_conf, ifelse(isBlank(curr_conf),"","&"), c,sep="")
		conf_expr <<- parse(text=conf)
		# goals = paste(getStr(getGoalNodes(bn), "True"), collapse="&")
		# goals_expr <<- parse(text=goals)
		# val_conf_goals = cpquery(bn, eval(goals_expr), eval(conf_expr), n=inference_param)

		prob_list = c()
		for(n in curr_act) {
			node_values = getNodeValues(bn, n)
			if("obeyed" %in% node_values) {
				e <<- parse(text=getStr(n, "obeyed"))
				val = cpquery(bn, eval(e), eval(conf_expr), n=INFERENCE_PARAM)
				prob_list = c(prob_list, val)
			}
		}
		if(length(prob_list)==0) {
			prob = 1
		}
		else {
			prob = mean(prob_list)
		}
	}

	r = c()
	n_new = getBestConf(bn, c, if_id, curr_act, curr_dis)

	# # if(!FAST_EXECUTION) print(curr_act)
	# if(!FAST_EXECUTION) print("best conf (n_new): ")
	# if(!FAST_EXECUTION) print(n_new)
	if(length(curr_act)==0) {
		# if(!FAST_EXECUTION) print("No norms were active. Suggested new configuration:")
		# if(!FAST_EXECUTION) print(n_new)
		if(all(is.na(n_new))) {
			# if(!FAST_EXECUTION) print("(No possible configuration of norms in actually better than disabling all of them).")
			if(!SCALABILITY_EXPERIMENTATION) {
				r = getRevisions(curr_dis, "d", if_id, c)
			}
		}
	}
	else {
		disabled = getDisabledNodesInConf(n_new, bn)
		disabled = setdiff(disabled, curr_dis)
		# if(!FAST_EXECUTION) print(disabled)
		#isThereAnyOneHarmful ? 
		if(length(disabled)>0) {
			#yes? then disable or relax
			# if(!FAST_EXECUTION) print("Seggested disabling (if a possibility) or relaxation of one or more of the following req (harmful: better when disabled):")
			# if(!FAST_EXECUTION) print(disabled)
			if(!SCALABILITY_EXPERIMENTATION) {
				r = getRevisions(disabled, "d", if_id, c)
			}
		}
		else {
			#no?
			goals_false = paste(getStr(problematic_goals, "False"), collapse="&")
			#n_e = getMLE(bn, getStr("O","False"), c)
			n_e = getMLE(bn, goals_false, c, curr_act, curr_dis)
			# if(!FAST_EXECUTION) print("n_e: ")
			# if(!FAST_EXECUTION) print(t(n_e))
			# # if(!FAST_EXECUTION) print(paste(colnames(n_e), n_e))

			ob_but_better_viol = vector()
			viol_and_better_viol = vector()
			viol_but_better_ob = vector()

			for(n in n_new) {
				n_des = getDesiredVal(bn, n, c,problematic_goals, curr_act, curr_dis)
				# if(!FAST_EXECUTION) print(paste("n_des (node ",n,"):",n_des))
				# # if(!FAST_EXECUTION) print(n_e)
				if(!is.na(n_e)) {
					if((n_e[1, n]=="obeyed") && (n_des=="violated"))
						ob_but_better_viol = union(ob_but_better_viol, n)
					if((n_e[1, n]=="violated") && (n_des=="violated"))
						viol_and_better_viol = union(viol_and_better_viol, n)
					if((n_e[1, n]=="violated") && (n_des=="obeyed"))
						viol_but_better_ob = union(viol_but_better_ob, n)
				}
				else {
					if(!FAST_EXECUTION) print("WARNING: n_e is NaN in reviseIF_Revised")
				}
				
			}
			# if(!FAST_EXECUTION) print(prob)
			# if(!FAST_EXECUTION) print(val_conf_goals)
			#in which sector are we?	
			if(prob>=good_req_satisfaction && val_conf_goals<good_goal_achievement) {
				#case 1: high prob sat, low goal ach
				# if(!FAST_EXECUTION) print("Current situation: acceptable/good average satisfaction of requirements, low goals acheivement")
				#is there anyone obeyed but better when violated?
				if(length(ob_but_better_viol)>0) {
					#yes? relax it to support violations
					# if(!FAST_EXECUTION) print("Seggested relaxation of one or more of the following req (likely obeyed but better when violated):")
					# if(!FAST_EXECUTION) print(ob_but_better_viol)
					if(!SCALABILITY_EXPERIMENTATION) {
						r = getRevisions(ob_but_better_viol, "r", if_id, c)
					}

				}
				else {
					#no? you need new req (add to decomposition, current goals are no harmful and good when obeyed)
					# if(!FAST_EXECUTION) print("Seggested introduction of new/diff norms: current ones are not harmful and they are good when obeyed, but not enough to achieve the goals")
					if(!SCALABILITY_EXPERIMENTATION) {
						r = union(getRevisions(n_new, "s2", if_id, c), getRevisions(n_new, "s1", if_id, c))
					}

				}
			}
			else if(prob<good_req_satisfaction && val_conf_goals<good_goal_achievement) {
				#case 2: low prob sat, low/acceptable goal ach
				# if(!FAST_EXECUTION) print("Current situation: low satisfaction of requirements, low goals acheivement")
				# if(!FAST_EXECUTION) print("Seggested strenghtening of one or more of the following req (not good but likely violated):")
				# if(!FAST_EXECUTION) print(n_new) #TODO: qui in realtà bisognerebbe dire tutti quelli che sono sotto il threshold di satisf
				# if(!FAST_EXECUTION) print("if agents or sanctions as repair: increase/introduce sanctions")
				# if(!FAST_EXECUTION) print("else: regimentation or introduce/change new reqs")
				if(!SCALABILITY_EXPERIMENTATION) {
					r = getRevisions(n_new, "s2", if_id, c)
				}

				## if(!FAST_EXECUTION) print(r)
			}
			else if(prob<good_req_satisfaction && val_conf_goals>=good_goal_achievement) {
				# if(!FAST_EXECUTION) print("Current situation: low satisfaction of requirements, acceptable goals acheivement")
				#is there anyone violated and good violated?
				#yes? disable/relax
				if(length(viol_and_better_viol)>0) {
					# if(!FAST_EXECUTION) print("Seggested disabling or relaxation of one or more of the following req (likely violated and good when violated):")
					# if(!FAST_EXECUTION) print(viol_and_better_viol)
					if(!SCALABILITY_EXPERIMENTATION) {
						r = getRevisions(viol_and_better_viol, "r", if_id, c)
					}

				}
							#no?
								#is there anyone violate but better when obeyed?
				else if(length(viol_but_better_ob)>0) {
					# if(!FAST_EXECUTION) print("Seggested strenghtening of one or more of the following req (likely violated but better when obeyed):")
					# if(!FAST_EXECUTION) print(viol_but_better_ob)
					# if(!FAST_EXECUTION) print("if agents or sanctions as repair: increase/introduce sanctions")
					# if(!FAST_EXECUTION) print("else: regimentation or introduce/change new reqs")
					if(!SCALABILITY_EXPERIMENTATION) {
						r = union(getRevisions(viol_but_better_ob, "s2", if_id, c), getRevisions(viol_but_better_ob, "s1", if_id, c))
					}

				}
			}
			else { #high prob sat, high prob ach
				# if(!FAST_EXECUTION) print("Current situation: high satisfaction of requirements, acceptable goals acheivement")
				# if(!FAST_EXECUTION) print("If not enough is seggested introduction of new/diff norms: current ones are good but not enough to achieve the goals. Also possible to try to strenghten them a bit more.")
				r = c(if_id)
			}			
		}
	}
	r
}



#' getRevisions
#' the function must determine a list of alternative configurations of requiremetns based on the suggestion of revision given.
#' It makes use of a matrix relationships_matrix generated in the initialization of the framework.
#'
#' @param norm the norms to revise.
#' @param type the revision suggested. a string, e.g., "r" for relaxation etc.
#' @param if_id the id of the current configuration
#' @param c the context, a string e.g. (C_T=="day")&(C_W=="extreme")
#'
#' @return the union of sugested configuration.
#' 
#' @examples
#' updateConf("N_1", "r", 1, "(C_T=="day")&(C_W=="extreme")")
#' 
getRevisions <- function(norms, type,  if_id, c) {
	# if(!FAST_EXECUTION) print("Searching for configurations where norms")
	# if(!FAST_EXECUTION) print(norms)
	# if(!FAST_EXECUTION) print(paste("are",type,"w.r.t. the current conf"))

	# relationships_matrix è una lista di matrici, una per ogni configurazione.
	# la i-esima matrice contiene, per ogni requisito, la sua revisione in ogni altra configurazione, rispetto a se stesso nella configurazione i-esima

	curr_conf_id = getConf(if_id, c)
	# if(!FAST_EXECUTION) print(paste("current configuration active in ", c, ":", curr_conf_id))
	if(curr_conf_id==-1) {
		print("WARNING: it says no configuration active, but why??????????????")
	}

	# # if(!FAST_EXECUTION) print(relationships_matrix[[curr_conf_id]])
	# print(type)
	r = c()
	if(length(norms)>0) {
		for(req in norms) {
			#data una norma e un tipo di revisione
			# determino le configurazioni in cui quella norma è revised nel modo suggerito
			val_of_req_in_all_other_confs = relationships_matrix[[curr_conf_id]][req,]
			for(other_conf_id in 1:length(val_of_req_in_all_other_confs)) {
				val = val_of_req_in_all_other_confs[other_conf_id]
				if((type=="r" && (val == "relaxed" || val == "disabled")) || (type=="d" && (val == "disabled")) ||
					(type=="s1" && (val == "strengthened")) || (type=="s2" && (val == "alteration")) || (type=="-" && (val == "-"))) {
					r = union(r, other_conf_id) #NOTO CHE QUA STO AGGIUNGENDO TUTTE LE CONFIGURAZIONI, PERò POTREBBERO NON AVERE UNA RELATIVA IF DISPONIBILE
				}
			}
		}
	}
	r
}

#' suggestRevision determines a suggestion of revision of a norm based on the currently frequent satisfaction of the norm. 
#' See document \url{C:\Users\DellA003\surfdrive\PhD\PhD Project\Notes\Document_Norms_Revision\norms_revision.pdf} for more details
#'
#' @param n the name of the norm
#' @param mle the most likely explanation (a string sat or viol) for n when the goal is not achieved
#' @param des_val the desired value for the norm, i.e. the value (a string sat or viol) that maximized the prob to achieve the goal
#' @param c the context
#' 
#' @return the suggested revision, a string "r", "s1" or "s2"
#' 
#' @examples
#' suggestRevision("N_1", "violated", "satisfied", paste(getStr("C_Hour", "rush")))
#' note that the above example can be executed only if in the BN there is a node N_1 and a context node C_Hour that can take value rush
#' 
suggestRevision <- function(n, mle, des_val, c) {
	sugg_rev = c("")
	if(des_val=="violated")  {
		# if(!FAST_EXECUTION) print("des_val: violated -> relax")
		sugg_rev = c("r")
	}
	else {
		if(!is.na(mle)) {
			if(mle[1, n]=="violated") {
				# if(!FAST_EXECUTION) print("des_val: obeyed, mle: violated -> strenghten sanction or relax norm")
				sugg_rev = c("s1")
			}
			else {
				# if(!FAST_EXECUTION) print("des_val: obeyed, mle: obeyed -> strenghten or nothing")
				sugg_rev = c("s1","s2")
			}
		}
		else {
			# print("WARNING: n_e is NaN in suggestRevision in reviseIF_FirstIdea")
			sugg_rev = c("s2")
		}

	}
	sugg_rev
}


#' getBestConf determines the best configuration of requirements (active/disabled) in a specific context according to the information collected until now
#'
#' @param bn the bayesian network, a bn.fit object
#' @param c the context, a string
#' 
#' @return a vector of the names of the norms active in the best estimated configuration
#' 
#' @examples
#' getBestConf(fitted_net, paste(getStr("C_Hour", "rush")))
#' note that the above example can be executed only if in the BN there is a context node C_Hour that can take value rush
#' 
getBestConf <- function(bn, c, if_id, curr_act, curr_dis) {
	best_conf_s = ""
	val_best_conf = -1
	best_conf = vector()
	goals = paste(getStr(getGoalNodes(bn), "True"), collapse="&")
	goals_expr <<- parse(text=goals)

	if((length(curr_act)==0) || allConfTried(c)) {
		# if(!FAST_EXECUTION) print("è questo il caso? lenght curr act 0 oppure allconftried")

		nodes.all = nodes(bn)
		nodes.n = nodes.all[grepl("^N+", nodes.all)]

		# # if(!FAST_EXECUTION) print("generate the powerset of all nodes")
		# ps = powerSetF(nodes.n)
		#
		# # if(!FAST_EXECUTION) print("cycling for all config...does this make any sense now? shouldn't it be based on the possible configurations???")
		# for(i in 1:length(ps)) {
		# 	conf_s = ""
		# 	conf = vector()
		# 	for(j in 1:length(nodes.n)) {
		# 		conf_s = paste(conf_s, ifelse(isBlank(conf_s),""," & "), ifelse(nodes.n[j] %in% ps[[i]], getStr(nodes.n[j], "active"), getStr(nodes.n[j], "disabled")),sep="")
		# 		conf = union(conf, ifelse(nodes.n[j] %in% ps[[i]], nodes.n[j], conf))
		# 	}
		# 	ce <<- parse(text=paste(conf_s, ifelse(isBlank(conf_s),""," & "), c, sep=""))
		# 	val_conf = cpquery(bn, eval(goals_expr), eval(ce), n=inference_param)
		# 	## if(!FAST_EXECUTION) print("check conf")
		# 	## if(!FAST_EXECUTION) print(ce)
		# 	## if(!FAST_EXECUTION) print("val")
		# 	## if(!FAST_EXECUTION) print(val_conf)
		# 	if(val_conf > val_best_conf) {
		# 		val_best_conf = val_conf
		# 		best_conf_s = conf_s
		# 		best_conf = conf
		# 	}
		# }

		#VERSIONE ALTERNATIVA A SOPRA CHE CONSIDERA SOLO TUTTE LE POSSIBILI GIA PREDEFINITE CONFIGURAZIONI
		for(i in 1:nrow(active_nodes_per_conf)) {
			conf_s = ""
			conf = vector()
			active_in_i = colnames(active_nodes_per_conf[i, active_nodes_per_conf[i,]==1])
			for(j in 1:ncol(active_nodes_per_conf)) {
				name = colnames(active_nodes_per_conf)[j]
				conf_s = paste(conf_s, ifelse(isBlank(conf_s),""," & "), ifelse(name %in% active_in_i, getStr(nodes.n[j], "active"), getStr(nodes.n[j], "disabled")),sep="")
				conf = union(conf, ifelse(name %in% active_in_i, name, conf))
			}
			conf = conf[!is.na(conf)]
			ce <<- parse(text=paste(conf_s, ifelse(isBlank(conf_s),""," & "), c, sep=""))
			val_conf = cpquery(bn, eval(goals_expr), eval(ce), n=INFERENCE_PARAM)
			## if(!FAST_EXECUTION) print("check conf")
			## if(!FAST_EXECUTION) print(ce)
			## if(!FAST_EXECUTION) print("val")
			## if(!FAST_EXECUTION) print(val_conf)
			if(val_conf > val_best_conf) {
				val_best_conf = val_conf
				best_conf_s = conf_s
				best_conf = conf
			}
		}
	}
	else {
		# if(!FAST_EXECUTION) print("lenght curr act maggiore di 0 e not allconftried")
		n_d = curr_dis
		n_a = curr_act

		if(length(curr_dis)>0){
			for(i in 1:length(n_d)) {
				best_conf_s = paste(best_conf_s, ifelse(isBlank(best_conf_s),""," & "), getStr(n_d[i], "disabled"), sep="")
			}
		}

		for(n in n_a) {
			# if(!FAST_EXECUTION) print(n)
			conf = getConfStringWithoutN(n_a, n, n_d, c)
			conf_n_ob = paste(conf, " & ", getStr(n, "obeyed"),sep="")
			conf_n_ob_expr <<- parse(text=conf_n_ob)
			# if(!FAST_EXECUTION) print(conf_n_ob_expr)
			val_conf_ob = cpquery(bn, eval(goals_expr), eval(conf_n_ob_expr), n=INFERENCE_PARAM)
			if(val_conf_ob > good_req_satisfaction) {
				conf_s = getConfStringWithoutN(n_a, n, n_d, c)
				conf_n_dis = paste(conf_s, ifelse(isBlank(conf_s),""," & "), getStr(n, "disabled"),sep="")
				conf_n_dis_expr <<- parse(text=conf_n_dis)
				conf_n_act = paste(conf_s, ifelse(isBlank(conf_s),""," & "), getStr(n, "active"),sep="")
				conf_n_act_expr <<- parse(text=conf_n_act)

				val_conf_dis = cpquery(bn, eval(goals_expr), eval(conf_n_dis_expr), n=INFERENCE_PARAM)
				val_conf_act = cpquery(bn, eval(goals_expr), eval(conf_n_act_expr), n=INFERENCE_PARAM)

				## if(!FAST_EXECUTION) print(paste(n, ": act ", val_conf_act, ", dis ", val_conf_dis))

				if(val_conf_dis > val_conf_act) {
					best_conf_s = paste(best_conf_s, ifelse(isBlank(best_conf_s),""," & "), getStr(n, "disabled"),sep="")
				}
				else {
					best_conf_s = paste(best_conf_s, ifelse(isBlank(best_conf_s),""," & "), getStr(n, "active"),sep="")
					best_conf = union(best_conf, n)
				}
			}
			else {
				best_conf = union(best_conf, n)
			}

			
		}
			## if(!FAST_EXECUTION) print(best_conf_s)

	}
	best_conf
}




#' getMLE determines the MLE for nodes N (norms) that are active in a context c
#'
#' @param bn a bn.fit object representing the Bayesian Network
#' @param obj_val the value of the overall goal for which we are asking the MLE (usually O == \"FALSE\")
#' @param c the context, a string
#' 
#' @return a matrix containing for each active norm the MLE when O has the value val
#' 
#' @examples
#' getMLE(fitted_net, "O == \"FALSE\"", paste(getStr("C_Hour", "rush")))
#' note that the above example can be executed only if in the fitted_net there is a goal O that can take value FALSE and a context node C_Hour that can take value rush
#' 
getMLE <- function(bn, obj_val, c, nodes.active, nodes.disabled) {
	# nodes.active = getCurrActiveNodes(bn, c)
	## if(!FAST_EXECUTION) print(nodes.active)
	## if(!FAST_EXECUTION) print(nodes.disabled)
	if(length(nodes.active)>0) {
		curr_conf = getCurrConf(bn, c, nodes.active, nodes.disabled)
		ev_s = paste(curr_conf,ifelse(isBlank(curr_conf),""," & "),c," & ",obj_val,sep="")
		e_p <<- parse(text=ev_s)
		## if(!FAST_EXECUTION) print(e_p)
		N_cpdits = cpdist(bn, nodes = (nodes.active), evidence = eval(e_p))
		## if(!FAST_EXECUTION) print(head(N_cpdits))
		#print(head(N_cpdits))
		t = prop.table(table(N_cpdits))
		#print(t)
		## if(!FAST_EXECUTION) print(head(t))
		w = which(t == max(t), arr.ind = TRUE)
		## if(!FAST_EXECUTION) print(w)
		print(w)
		cols = colnames(w)
		## if(!FAST_EXECUTION) print(cols)
		## if(!FAST_EXECUTION) print(nrow(w))
		if(nrow(w)>0) {
			for (i in 1:length(cols)) {
				col = cols[i]
				if(!is.nan(w[1, i])) {
					ind = as.integer(w[1, i]) #indice del valore nella tabella w per la colonna col
					# # if(!FAST_EXECUTION) print(ind)
					# # if(!FAST_EXECUTION) print(labels(t))
					# # if(!FAST_EXECUTION) print(labels(t)[col])
					# # if(!FAST_EXECUTION) print(labels(t)[col][[1]])
					w[1,i] = labels(t)[col][[1]][ind]
				}
				else {
					print("WARNING: NaN values in the cpdist, during MLE calculation")
				}
			}
			if(length(colnames(w))==1) {
				colnames(w) = nodes.active
				rownames(w) = c("MLE")
			}

			w
		}
		else {
			print(paste("WARNING: empty cpdist, during MLE calculation. Can't find an explanation for ", e_p, ". This situation never occurred."))
			matrix()
		}
	}
	else {
		matrix()
	}
}


#' getDesiredVal determines the the desired value (obeyed or violated) in c for node n
#'
#' @param bn a bn.fit object representing the Bayesian Network
#' @param node the node for which we are asking the desired value, a string with the name
#' @param c the context, a string
#' 
#' @return a string obeyed or violated for node node
#' 
#' @examples
#' getDesiredVal(fitted_net, "N_1", paste(getStr("C_Hour", "rush")))
#' note that the above example can be executed only if in the fitted_net there is a node N and a context node C_Hour that can take value rush
#' 
getDesiredVal <- function(bn, node, c,problematic_goals, n_a, n_d) {
	conf = getConfString(n_a, n_d, c)
	conf_n_ob = paste(conf, " & ", getStr(node, "obeyed"),sep="")
	conf_n_ob_expr <<- parse(text=conf_n_ob)
	conf_n_viol = paste(conf, " & ", getStr(node, "violated"),sep="")
	conf_n_viol_expr <<- parse(text=conf_n_viol)

	# goals = paste(getStr(getGoalNodes(bn), "True"), collapse="&")
	goals = paste(getStr(problematic_goals, "True"), collapse="&")
	goals_expr <<- parse(text=goals)

	val_conf_ob = cpquery(bn, eval(goals_expr), eval(conf_n_ob_expr), n=INFERENCE_PARAM)
	val_conf_viol = cpquery(bn, eval(goals_expr), eval(conf_n_viol_expr), n=INFERENCE_PARAM)

	# # if(!FAST_EXECUTION) print(paste("val ob", val_conf_ob))
	# # if(!FAST_EXECUTION) print(paste("val viol", val_conf_viol))
	node_values = getNodeValues(bn, node)
	if("obeyed" %in% node_values || "violated" %in% node_values) {
		if(val_conf_ob >= val_conf_viol) {
			return("obeyed")
		}
		else { return("violated")}
	}
	else return("active")

}

#' getCurrConf returns a logical expression (a string) for nodes in N describing the current configuration
#'
#' @param bn a bn.fit object representing the Bayesian Network
#' @param c the context, a string
#' 
#' @return a logical expression (a string) for nodes in N, e.g., "(N_1 == disabled) & (!(N_2 == disabled)) & ..."
#' 
#' @examples
#' getDesiredVal(fitted_net, paste(getStr("C_Hour", "rush")))
#' note that the above example can be executed only if in the fitted_net there is a context node C_Hour that can take value rush
#' 
getCurrConf <- function(bn, c, curr_act, curr_dis) {
	x = ""
	#if(!SCALABILITY_EXPERIMENTATION) {
	if(length(curr_act)>0)	{
		for(i in 1:length(curr_act)) {
			x = paste(x, ifelse(isBlank(x),"","&"), getStr(curr_act[i], "active"),sep="")
		}
	}
	if(length(curr_dis)>0)	{
		for(i in 1:length(curr_dis)) {
			x = paste(x, ifelse(isBlank(x),"","&"), getStr(curr_dis[i], "disabled"),sep="")
		}
	}
	#}
	x
}



#' getOverallGoalSatisf determines, by querying the BN, the probability of satisfaction of all the overall objectives in a specific context
#'
#' @param bn a bn.fit object representing the Bayesian Network
#' @param c the context, a string
#' @param active_norms a vector of names of the active norms in context c
#' 
#' @return a probability (a numerical value) representing the probability of satisfaction of all the overall objectives in context c
#' 
#' @examples
#' getOverallGoalSatisf(fitted_net, paste(getStr("C_Hour", "rush")), "N_1", "")
#' note that the above example can be executed only if in the fitted_net there is a node N and a context node C_Hour that can take value rush
#' 
getOverallGoalSatisfInContext <- function(bn, c, active_norms, disabled_norms) {
	# setCurrActiveNodes(active_norms)
	# curr_act = getCurrActiveNodes(bn, c)
	# curr_dis = getCurrDisabledNodes(bn, c)
	curr_conf = getCurrConf(bn,c, active_norms, disabled_norms)
	conf = paste(curr_conf, ifelse(isBlank(curr_conf),"","&"), c,sep="")
	conf_expr <<- parse(text=conf)
	goals = paste(getStr(getGoalNodes(bn), "True"), collapse="&")
	goals_expr <<- parse(text=goals)
	cpquery(bn, eval(goals_expr), eval(conf_expr), n=INFERENCE_PARAM)
}


#' getOverallSingleGoalSatisfInContext determines, by querying the BN, the probability of satisfaction of one overall objective in a specific context
#'
#' @param bn a bn.fit object representing the Bayesian Network
#' @param c the context, a string
#' @param active_norms a vector of names of the active norms in context c
#' @param objective the name of the objective to evaluate
#' 
#' @return a probability (a numerical value) representing the probability of satisfaction of the overall objective in context c
#' 
#' @examples
#' getOverallSingleGoalSatisfInContext(fitted_net, paste(getStr("C_Hour", "rush")), "N_1", "", "O_1")
#' note that the above example can be executed only if in the fitted_net there is a node N and a context node C_Hour that can take value rush and a node O_1 that can take value "True"
#' 
getOverallSingleGoalSatisfInContext <- function(bn, c, active_norms, disabled_norms, objective) {
	curr_conf = getCurrConf(bn,c, active_norms, disabled_norms)
	conf = paste(curr_conf, ifelse(isBlank(curr_conf),"","&"), c,sep="")
	conf_expr <<- parse(text=conf)
	goal = getStr(objective, "True")
	goal_expr <<- parse(text=goal)
	cpquery(bn, eval(goal_expr), eval(conf_expr), n=INFERENCE_PARAM)
}

setGoodReqSatisfaction <- function(good_req_sat) {
	good_req_satisfaction <<- good_req_sat;	
}
setGoodGoalAchievement <- function(good_goal_ach) {
	good_goal_achievement <<- good_goal_ach;	
}



#' getStr generate a well formed string to give to the BN library as evidence or query
#'
#' @param n the name of a variable in the network
#' @param val the value to give to n
#' 
#' @return a well formed string such as (n=="val"). Notice that if val is "active" then it returns (!(n=="disabled")), since the norm could still have either satisfied or violated val.
#' 
#' @examples
#' getStr("N_1", "active")
#' 
getStr <- function(n, val) {
	if(val=="disabled") {
		paste("(", n, "==\"disabled\")",sep="")
	}
	else if(val=="active") {
		#if(SCALABILITY_EXPERIMENTATION) {
		#	paste("(!(", n, "==\"active\"))",sep="")
		#}
		#else {
		paste("(!(", n, "==\"disabled\"))",sep="")
		#}
	}
	else paste("(", n, "==\"",val,"\")",sep="")
}







#' exec simulates an execution of the system and returns the probability of satisfaction of the overall objective in a specific context with the specified configuration
#'
#' @param fit a bn.fit object representing the Bayesian Network
#' @param context the context, a string
#' @param conf the configuration, a vector of names of the active norms
#' 
#' @return the probability of satisfaction of the overall objective in context with the configuration conf
#' 
#' @examples
#'  exec(currBN, context, getActiveNormsPerConf(currSol, context, active_nodes_per_conf))
#' 
exec <- function(fit, context, active, disabled) {
	getOverallGoalSatisfInContext(fit, context, active, disabled)

}


#' getGoalNodes
#' 
#' @param bn is a bn.fit object representing the current Bayesian Network
#' 
#' @return the set of names of the nodes representing the goals
#' 
#' @examples
#' getGoalsNodes(fitted_bn)
#' 
getGoalNodes <- function(bn) {
	nodes.all = nodes(bn)
	nodes.all[grepl("^O+", nodes.all)]
}

#' getContextNodes
#' 
#' @param bn is a bn.fit object representing the current Bayesian Network
#' 
#' @return the set of names of the nodes representing contextual variables
#' 
#' @examples
#' getContextNodes(fitted_bn)
#' 
getContextNodes <- function(bn) {
	nodes.all = nodes(bn)
	nodes.all[grepl("^C+", nodes.all)]
}


#' getDisabledNodesInConf
#'
#' @param conf is a set of names of nodes representing the active requirements in the configuration
#' @param bn is a bn.fit object representing the current Bayesian Network with
#'	all the information collected until now (see function generateBN to obtain such an object)
#'
#' @return the set of names of the remaining requirements nodes in the configuration that therefore are disabled.
#'
#' @examples
#' getDisabledNodesInConf("N_1", generateBN(1))
#' note that the above example can be executed only if there is a configuration with id 1 with at least a node N_1
#'
getDisabledNodesInConf <- function(conf, bn) {
	nodes.all = nodes(bn)
	nodes.n = nodes.all[grepl("^N+", nodes.all)]
	setdiff(nodes.n, conf)
}



getSuggestedNewConfig <- function(bn, context, problematic_goals, currIf, currAct, currDis, good_req_sat, t, algo_version, symbol) {
    # TODO distinguere casi diversi, con diverse alternative per ottenere new config per un context
    # implementare recProc
    # NOTA che tutte le funzioni qua sotto devono ritornare una lista di CONFIGURAZIONI suggerite per un certo contesto
    reviseIF(bn, context, problematic_goals, currIf, currAct, currDis, good_req_sat, t, algo_version, symbol)
}




