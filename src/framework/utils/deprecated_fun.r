


#' updateIF updates the institutional facts in the framework, i.e. concretely set the new choosen configuration.
#' See document \url{C:\Users\DellA003\surfdrive\PhD\PhD Project\Notes\Document_Norms_Revision\norms_revision.pdf} for more details
#' N.B. this function now is never called
#'
#' @param bn the bayesian network, a bn.fit object
#' @param conf the new configuration, a vector of strings of names of active norms in the new configuration
#' @param c the context, a string
#'
#' @return nothing
#'
#' @examples
#' updateIF(fitted_net, "N_1", paste(getStr("C_Hour", "rush")))
#' note that the above example can be executed only if in the BN there is a norm "N_1" and a context node C_Hour that can take value rush
#'
#' TODO: NOTA, NON è USATA DA NESSUNA PARTE
updateIF <- function(bn, conf, c) {
	conf_s = paste(conf, ifelse(isBlank(conf),"","&"), c , sep="")
	conf_e <<- parse(text=conf_s)
	if(cpquery(bn, (O=="True"), eval(conf_e), n=INFERENCE_PARAM)>=good_goal_achievement || isNew(conf, c) || allConfTried(c)) {
		# if(!FAST_EXECUTION) print(paste("setto la conf: ",conf))
		setIF(getContextId(c),getConfId(conf))
	}
	else {
		# if(!FAST_EXECUTION) print(cpquery(bn, (O=="True"), eval(parse(text=conf_s)), n=inference_param))
		# if(!FAST_EXECUTION) print("getNewConf TODO")
	}
}

#' updateConf
#' the function must update a given configuration by revising a norm n according to the revision suggested
#' Notice that this function is only used by reviseIF_FirstIdea
#'
#' @param n the name of the norm to revise
#' @param rev the revision suggested. a string, e.g., "r" for relaxation etc.
#' @param conf_id the id of the current configuration
#'
#' @return the new (or more than one) updated configuration. Now temporary returns a list of ids of the possible new configurations
#'
#' @examples
#' updateConf("N_1", "N_1", "r", 1)
#' note that the above example can be executed only if there is a configuration with id 1 with at least a node N_1 which is currently active in conf 1
#'
updateConf <- function(n, rev, if_id, c) {
	possible_revisions = getRevisions(n, rev, if_id, c)
	possible_revisions
}


#' setIF is just a sub-function that actually performs the update to the IF. It also logs the previous IF, otherwise lost
#'
#' @param context_id the id of the context for which the IF are updated
#' @param conf_id the id of the configuration set for the context
#'
#' @return nothing
#'
#' @examples
#' setIF(1,1)
#'
setIF <- function(context_id, conf_id) {
	# if(!FAST_EXECUTION) print("================LOG: PREVIOUS IF================")
	# if(!FAST_EXECUTION) print(IF)
	# if(!FAST_EXECUTION) print("================================================")
	IF[context_id,] <<- 0
	IF[context_id,conf_id] <<- 1
}




setCurrActiveNodes <- function(nodes) {
	curr_active_nodes <<- nodes;	
}

#' getCurrActiveNodes determines the currently active requirements in a given context, i.e. the configuration used in a context. TODO
#'
#' @param bn a bn.fit object representing the Bayesian Network
#' @param c the context, a string
#' 
#' @return a vector of strings (the labels of the nodes currently active)
#' 
#' @examples
#' getCurrActiveNodes(fitted_net, paste(getStr("C_Hour", "rush")))
#' note that the above example can be executed only if in the fitted_net there is one and only one context node C_Hour that can take value rush
#' 
getCurrActiveNodes <- function(bn, c) {
	conf_id = getIF(c)
	getActiveNormsPerConf(conf_id+1, active_nodes_per_conf)
}

#' getCurrDisabledNodes determines the currently disabled requirements in a given context
#'
#' @param bn a bn.fit object representing the Bayesian Network
#' @param c the context, a string
#' 
#' @return a vector of strings (the labels of the nodes currently disabled)
#' 
#' @examples
#' getCurrDisabledNodes(fitted_net, paste(getStr("C_Hour", "rush")))
#' note that the above example can be executed only if in the fitted_net there is a context node C_Hour that can take value rush
#' 
getCurrDisabledNodes <- function(bn, c) {
	nodes.all = nodes(bn)
	nodes.n = nodes.all[grepl("^N+", nodes.all)]
	setdiff(nodes.n, getCurrActiveNodes(bn, c))
}

getActiveSize <- function(bn, c) {
	length(getCurrActiveNodes(bn, c))
}
getDisabledSize <- function(bn, c) {
	length(getCurrDisabledNodes(bn, c))
}


getInitNetworkUniformData <- function() {
	active_nodes_per_conf = read.table("configurations_full.csv", header = TRUE, sep = "\t")

	CONTEXTS = c("C_W","C_T")
	reqs = c("N_N",	"N_NP",	"N_NPS", "N_NPA", "N_NN", "N_J", "N_JT", "N_JTS", "N_JTA", "N_JN")
	params = c("V_NPAP")
	goals= c("O_1","O_2","O_N1","O_N2","O_N3")

	sequence= c("C_W","C_T","N_NP","N_NPA","V_NPAP","N_NPS","N_NN","N_J","N_JT","N_JTA","N_JTS","N_JN","O_1","O_2","N_N","O_N1","O_N2","O_N3")

	C_W <- c("normal", "extreme")
	C_T <- c("day", "night")
	V_NPAP <- c("v00","v01","v02","v03","v04")
	O_1 <- c("True","False")
	O_2 <- c("True","False")
	O_N1 <- c("True","False")
	O_N2 <- c("True","False")
	O_N3 <- c("True","False")

	for(conf_id in 1:nrow(active_nodes_per_conf)) {
		df = expand.grid(C_W, C_T,
		getArrayAllpossibleVals(active_nodes_per_conf, conf_id, "N_NP"),
		getArrayAllpossibleVals(active_nodes_per_conf, conf_id, "N_NPA"),
		V_NPAP,
		getArrayAllpossibleVals(active_nodes_per_conf, conf_id, "N_NPS"),
		getArrayAllpossibleVals(active_nodes_per_conf, conf_id, "N_NN"),
		getArrayAllpossibleVals(active_nodes_per_conf, conf_id, "N_J"),
		getArrayAllpossibleVals(active_nodes_per_conf, conf_id, "N_JT"),
		getArrayAllpossibleVals(active_nodes_per_conf, conf_id, "N_JTA"),
		getArrayAllpossibleVals(active_nodes_per_conf, conf_id, "N_JTS"),
		getArrayAllpossibleVals(active_nodes_per_conf, conf_id, "N_JN"),
		O_1,O_2,
		getArrayAllpossibleVals(active_nodes_per_conf, conf_id, "N_N"),
		O_N1,O_N2,O_N3, stringsAsFactors=FALSE)

		write.table(df, file = "data", append = TRUE,quote=FALSE, sep = "\t")
	}

}


getArrayAllpossibleVals <- function(active_nodes_per_conf, conf_id, node) {
	if(node %in% c("N_N",	"N_NP",	"N_NPS", "N_NPA", "N_NN")) {
		if (active_nodes_per_conf[conf_id,node]==1) {
			c("obeyed","violated","disabled")
		}
		else {
			c("disabled")
		}
	}
	else {
		if (active_nodes_per_conf[conf_id,node]==1) {
			c("active","disabled")
		}
		else {
			c("disabled")
		}
	}
}



# getRevisions <- function(norms, type,  if_id, c, curr_act, curr_dis) {
# 	r = c()
# 	conf=-1 #temp TODO
# 	if(type=="r") { #relaxation
# 		if(conf==0) r = c(12)
# 		else if(conf>=1 && conf<=5)  r = c(conf-1, 12)
# 		else if(conf==6)  r = c(12)
# 		else if(conf>=7 && conf<=11)  r = c(conf-1, 12)
# 		else if(conf==12) r = c(12)
# 	}
# 	else if(type=="s1"){
# 		if(conf>=0 && conf<=4)  r = c(conf+1, 12)
# 		else if(conf==5)  r = c(12)
# 		else if(conf>=6 && conf<=10)  r = c(conf+1, 12)
# 		else if(conf==11)  r = c(12)
# 		else if(conf==12) r = c(0:12)
# 	}
# 	else if(type=="s2") {
# 		if(conf>=0 && conf<=5)  r = c(6:11)
# 		else if(conf>=6 && conf<=11)  r = c(0:5)
# 		else r = c(0:11)
# 	}
# 	r
# }


# DISTANCE MUST BE CALCULATED TAKING INTO ACCOUNT EACH CONTEXT, I.E. IT CAN BE CALCULATED FROM THE MATRIX IF (TODO)
# comparing 2 different if
dist <- function(confA, confB) {
	d = 1
	if(confA==0 && confB!=1 && confB!=12) d = 2
	if(confA>=1 && confA<=4 && confB!=confA-1 && confB!=confA+1 && confB!=12)  d = 2
	if(confA==5 && confB!=4 && confB!=12)  d = 2
	if(confA==6 && confB!=7 && confB!=12)  d = 2
	if(confA>=7 && confA<=10 && confB!=confA-1 && confB!=confA+1 && confB!=12)  d = 2
	if(confA==11 && confB!=10 && confB!=12)  d = 2
	d
}



fixDatasets <- function(num_if,dataset_per_if) {
	# if(!FAST_EXECUTION) print("Fixing datasets in order to avoid missing values...")
	new_folder = "fixed\\"
	new_datasets_folder = paste(DATASETS_FOLDER,new_folder, sep="")
	dir.create(file.path(DATASETS_FOLDER, new_folder))
	m = matrix(c("normal", "extreme", "normal",
	"day", "night", "day",
	"obeyed","violated","disabled",
	"obeyed","violated","disabled",
	"v00", "v01", "v02",
	"obeyed","violated","disabled",
	"obeyed","violated","disabled",
	"active","disabled","active",
	"active","disabled","active",
	"active","disabled","active",
	"active","disabled","active",
	"active","disabled","active",
	"True","False", "True",
	"True","False","True",
	"obeyed","violated","disabled",
	"True","False","True",
	"True","False","True",
	"True","False","True"), nrow=18, ncol=3,byrow = TRUE)

	for(i in 1:num_if) {
		outFile <- file(paste(new_datasets_folder,dataset_per_if[i], sep=""), "w")
		x <- readLines(paste(DATASETS_FOLDER,"IF1.txt",sep=""))
		writeLines(x[1:(length(x)-1)], outFile)
		FF <- as.matrix(t(m[,1]))
		write.table(FF, file = outFile, col.names = FALSE, row.names = FALSE, append=TRUE, quote=FALSE, sep = "	")
		FF <- as.matrix(t(m[,2]))
		write.table(FF, file = outFile, col.names = FALSE, row.names = FALSE, append=TRUE, quote=FALSE, sep = "	")
		FF <- as.matrix(t(m[,3]))
		write.table(FF, file = outFile, col.names = FALSE, row.names = FALSE, append=TRUE, quote=FALSE, sep = "	")
		close(outFile)
	}
	new_datasets_folder
}



#qui bisogna decidere a che livello: se a livello di if o di conf
# neighbors <- function(conf, all) {
# 	n = c()
# 	if(all==TRUE) {
# 		n=c(0:12)
# 	}
# 	else {
# 		if(conf==0) n = c(1, 12)
# 		else if(conf>=1 && conf<=4)  n = c(conf-1, conf+1, 12)
# 		else if(conf==5)  n = c(4, 12)
# 		else if(conf==6)  n = c(7, 12)
# 		else if(conf>=7 && conf<=10)  n = c(conf-1, conf+1, 12)
# 		else if(conf==11)  n = c(10, 12)
# 		else if(conf==12) n = c(0:11)
# 	}
# 	n
# }


#' returns a MATRIX REPRESENTING THE ACTIVE CONFIGURATION OF ACTIVE REQUIREMENTS FOR EACH OPERATING CONTEXT
getAllIF <- function() {
	IF
}