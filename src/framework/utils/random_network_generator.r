library(bnlearn)
setwd("C:\\Users\\DellA003\\surfdrive\\PhD\\PhD Project\\Stuff\\Softwares\\R\\bnlearn")
source("functions.R")
set.seed(1165)

nr_requirements = 1
avg_nr_version_per_req = 5
nr_goals = 1
nr_contextual_var = 10
avg_nr_values_contextual_var = 5

net = model2network("[C][N|C][O|N:C]")





generate_net_string <- function(nr_goals, nr_req, nr_cont, hierarchy) {
	s = ""
	if(hierarchy) {

	}
	else {
		list_of_parent_contexts = ""
		list_of_parent_norms = ""
		list_of_parent_norms_versions = ""
		for(i in 1:nr_cont) {
			c = paste("C_",i, sep="")
			list_of_parent_contexts = paste(list_of_parent_contexts, c, sep=":")
		}
		for(i in 1:nr_req) {
			n = paste("N_",i, sep="")
			list_of_parent_norms = paste(list_of_parent_norms, n, sep=":")
			v = paste("V_N_",i, sep="")
			list_of_parent_norms_versions = paste(list_of_parent_norms_versions, v, sep=":")
		}
		for(i in 1:nr_cont) {
			#GENERATE STRING FOR CONTEXT
			s = paste(s, "[C_",i,"]", sep="")
		}
		for(i in 1:nr_req) {
			#GENERATE STRING FOR VERSION OF REQ
			s = paste(s, "[V_N_",i,"]", sep="")
			#PARENTS OF A NORM: ITS VERSION AND ALL THE CONETXTS
			s = paste(s, "[N_",i,"|V_N_",i,list_of_parent_contexts,"]", sep="")
		}
		for(i in 1:nr_goals) {
			#PARENTS OF A GOAL: ALL NORMS AND ALL THE CONETXTS
			s = paste(s, "[O_",i,"|",substring(list_of_parent_contexts, 2),list_of_parent_norms,list_of_parent_norms_versions,"]", sep="")
		}
		# if(!FAST_EXECUTION) print(s)
	}
}