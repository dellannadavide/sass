#' generateBN generates a BN.
#'
#' @param conf the configuration, a vector of names of the active norms
#' 
#' @return a bn.fit object, a bayesian network with also data taken from the dataset associated to the configuration conf
#' 
#' @examples
#'  generateBN("N_1")
#' 
generateBN <- function(dataset, net_model) {
	#dataset = getDatasetPerConf(conf, dataset_per_conf)
	## if(!FAST_EXECUTION) print(paste("-----------------------", "Req: ",dataset,"-----------------------"))
	#net = model2network("[C_Hour][N_1|C_Hour][N_2|C_Hour][O|C_Hour:N_1:N_2]")
	# dataset = "LOG_test_crowdnav.txt"
	net = model2network(net_model)
	#leggo il dataset
	req_dataset = read.table(dataset, header = TRUE)

	#fix missing values (hardcoded)
	# req_dataset = fixMissingValues(req_dataset)

	fit = bn.fit(net, req_dataset)
	temp_grain = as.grain(fit) #to normalize missing params
	fit = as.bn.fit(temp_grain) #to normalize missing params
	# write.dsc("netica.dsc", fit)
	fit
}
#
# fixMissingValues <- function(dataset) {
# 	#FIX MISSING VALUES
# 	m = matrix(c("normal", "extreme", "normal",
# 				"day", "night", "day",
# 				"obeyed","violated","disabled",
# 				"obeyed","violated","disabled",
# 				"v00", "v01", "v02",
# 				"obeyed","violated","disabled",
# 				"obeyed","violated","disabled",
# 				"active","disabled","active",
# 				"active","disabled","active",
# 				"active","disabled","active",
# 				"active","disabled","active",
# 				"active","disabled","active",
# 				"True","False", "True",
# 				"True","False","True",
# 				"obeyed","violated","disabled",
# 				"True","False","True",
# 				"True","False","True",
# 				"True","False","True"), nrow=18, ncol=3,byrow = TRUE)
#
# 	# if(!FAST_EXECUTION) print(noquote(m))
# 	dataset = rbind(dataset, noquote(m[,1]))
# 	dataset = rbind(dataset, noquote(m[,2]))
# 	dataset = rbind(dataset, noquote(m[,3]))
# 	dataset
# 	######
#
# 	outFile <- file("IFPRVA", "w")
# 	x <- readLines(paste(DATASETS_FOLDER,"IF1.txt",sep=""))
# 	writeLines(x[1:(length(x)-1)], outFile)
# 	FF <- as.matrix(t(m[,1]))
# 	write.table(FF, file = outFile,
# 	col.names = FALSE, row.names = FALSE, append=TRUE, quote=FALSE, sep = "	")
# 	close(outFile)
#
# 	for (i in ListOfFileNames) {
# 		x <- readLines(paste(dataset_folder,i,sep=""))
# 		if(first) {
# 			writeLines(x[1:(length(x)-1)], outFile)
# 			first = FALSE
# 		}
# 		else{
# 			writeLines(x[2:(length(x)-1)], outFile)
# 		}
# 	}
# 	close(outFile)
# }


#' getOverallGoalSatisf determines, by querying the BN, the probability of satisfaction of the overall objective
#'
#' @param bn a bn.fit object representing the Bayesian Network
#' 
#' @return a probability (a numerical value) representing the probability of satisfaction of the overall objective
#' 
#' @examples
#' getOverallGoalSatisf(fitted_net)
#' 
getOverallGoalSatisf <- function(bn) {
	goals = paste(getStr(getGoalNodes(bn), "True"), collapse="&")
	goals_expr <<- parse(text=goals)
	cpquery(bn, eval(goals_expr), evidence = TRUE)
}

#' getAllPossibleContexts retrieves all possible combinations of values of the context variables
#'
#' @param bn a bn.fit object representing the Bayesian Network
#' 
#' @return a set of lists of assignments of values to each context variable
#' 
#' @examples
#'  getAllPossibleContexts(currBN)
#' 
getAllPossibleContexts <- function(bn) {
	set = c()
	n_cpdist = cpdist(bn, nodes = getContextNodes(bn), evidence =TRUE)
	t = prop.table(table(n_cpdist))
	df = expand.grid(dimnames(t), stringsAsFactors=FALSE)
	for(i in 1:ncol(df)) {
		var = colnames(df)[i]
		for(j in 1:nrow(df)) {
			df[j,i] = paste("(",var, "==\"", df[j,i], "\")",sep="")
		}
	}
	df$x = apply( df[,] , 1 , paste , collapse = "&" )
	df = df[ , ( names( df ) %in% c("x") ) ]
	df
}


getAllChildrenReq <- function(bn, node_name) {
    children(bn,node_name)[grepl("^N+", children(bn,node_name))]
}
getAllParentsReq <- function(bn, node_name) {
    parents(bn,node_name)[grepl("^N+", parents(bn,node_name))]
}
getAllTopLevelReq <- function(bn, reqs) {
    top_level_req = c()
    for(req in reqs)
        if(length(getAllChildrenReq(bn,req))==0)
            top_level_req = c(top_level_req, req)
    top_level_req
}


calculateAllReturns <- function(num_if,dataset_per_if) {
	# if(!FAST_EXECUTION) print("Calculate all returns ...")
	ret= c()
	for(i in 1:num_if) {
		# if(!FAST_EXECUTION) print(paste("IF",i))
		dataset = dataset_per_if[i]
		bn = generateBN(file.path(DATASETS_FOLDER, dataset), NET_MODEL)
		ret = c(ret, calculateSingleReturn(bn, CONTEXTS, i))
	}
	ret
}

calculateSingleReturn <- function(bn, contexts, curr_if) {
	ret = 0
	for(context in contexts) {
		ret = ret + exec(bn, context, getActiveNormsPerConf(curr_if, context, active_nodes_per_conf), getDisabledNormsPerConf(curr_if, context, active_nodes_per_conf))
	}
	ret = ret/length(contexts)
	ret
}


calculateMatrixReturns <- function(num_if,dataset_per_if) {
	# if(!FAST_EXECUTION) print("Calculate all returns ...")
	ret= c()
	for(i in 1:num_if) {
		print(paste("IF",i))
		dataset = dataset_per_if[i]
		bn = generateBN(file.path(DATASETS_FOLDER, dataset), NET_MODEL)
		goals = getGoalNodes(bn)
		ret = c(ret, calculateSingleMatrixReturn(bn, goals, CONTEXTS, i))
	}
	ret
}
calculateSingleMatrixReturn <- function(bn, goals, contexts, curr_if) {
	ret_list = c(0)
	for(context in contexts) {
		#overall goals achievement per context
		global_context = getOverallGoalSatisfInContext(bn, context, getActiveNormsPerConf(curr_if, context, active_nodes_per_conf), getDisabledNormsPerConf(curr_if, context, active_nodes_per_conf))
		ret_list[1] = ret_list[1] + global_context
		ret_list = c(ret_list, global_context)
		#specific goals achievement in each context
		for(j in 1:length(goals)) {
			ret_list = c(ret_list, getOverallSingleGoalSatisfInContext(bn, context, getActiveNormsPerConf(curr_if, context, active_nodes_per_conf), getDisabledNormsPerConf(curr_if, context, active_nodes_per_conf), goals[j]))
		}

		#avg probability of  satisfaction of the requirements
		curr_act = getActiveNormsPerConf(curr_if, context, active_nodes_per_conf)
		curr_conf = getCurrConf(bn,context, curr_act, getDisabledNormsPerConf(curr_if, context, active_nodes_per_conf))
		conf = paste(curr_conf, ifelse(isBlank(curr_conf),"","&"), context,sep="")
		conf_expr <<- parse(text=conf)
		prob_list = c()
		for(n in curr_act) {
			node_values = getNodeValues(bn, n)
			if("obeyed" %in% node_values) {
				e <<- parse(text=getStr(n, "obeyed"))
				val = cpquery(bn, eval(e), eval(conf_expr), n=INFERENCE_PARAM)
				prob_list = c(prob_list, val)
			}
		}
		if(length(prob_list)==0) {
			prob = 1
		}
		else {
			prob = mean(prob_list)
		}
		ret_list = c(ret_list, prob)
	}
	#average goals achievement in all contexts
	ret_list[1] = ret_list[1]/length(contexts)
	print(ret_list[1])
	ret_list
}
