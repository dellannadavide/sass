source("utils\\utils_bn.r")
source("utils\\utils_conf.r")
source("utils\\utils_if.r")


#' getContextId determines the id of a given context
#'
#' @param context the context, a string representing an assignment of values of ALL the contextual variables
#' 
#' @return the id of the context, or 0 if the context has not been found
#' 
#' @examples
#' getConfId(paste(getStr("C_Hour", "rush")))
#' note that the example above works only if in the bn there is one and only one contextual variable (C_Hour)
#' 
getContextId <- function(cont) {
	id=0
	for(i in 1:length(CONTEXTS)) {
		if(CONTEXTS[i]==cont) {
			id = i
			break
		}
	}
	if(id==0) {
		if(!FAST_EXECUTION) print(paste("context",cont,"not found"))
	}
	id
}


generateNewDataset <- function(dataset_folder, currDataset, newIFId) {
	# # if(!FAST_EXECUTION) print(newIFId)
	newIFFileName = dataset_per_if[newIFId]
	if(isBlank(currDataset))
		ListOfFileNames = c(newIFFileName)
	else
		ListOfFileNames = c(currDataset, newIFFileName)
	# ## if(!FAST_EXECUTION) print(newIFFileName)
	# newIFName = gsub(".txt", "", gsub("IF", "", newIFFileName))
	# ## if(!FAST_EXECUTION) print(newIFName)
	# old_name = gsub(".txt", "", gsub("IF", "", currDataset))
	# ## if(!FAST_EXECUTION) print(old_name)
	# new_name = paste("IF",old_name, "_", newIFName,".txt",sep="")
	# ## if(!FAST_EXECUTION) print(new_name)
	# new_file = paste(dataset_folder,new_name,sep="")

	new_name = paste("_", newIFFileName,sep="")
	new_file = file.path(dataset_folder,new_name)

	## if(!FAST_EXECUTION) print(new_file)
	outFile <- file(new_file, "w")
	first = TRUE
	for (i in ListOfFileNames) {
	    x <- readLines(file.path(dataset_folder,i))
	    if(first) {
	    	writeLines(x[1:(length(x)-1)], outFile)
	    	first = FALSE 
	    }
	    else{
		    writeLines(x[2:(length(x)-1)], outFile) 
	    }
	} 
	close(outFile)
	file.remove(file.path(dataset_folder, currDataset))
	new_name
}

# NOTA ANCHE CHE CON PIU GOAL BISOGNA GESTIRE LA SITUAZIONE IN QUALCHE MODO,
# AD ESEMPIO UN SOLO VETTEORE DI RETURN CON LA MEDIA OPPURE TANTI VETTORI QUANTI GOAL
evalu <- function(currSol) {
	if(FAST_EXECUTION) {
		RETURNS[currSol]
	}
	else {
		currBN = generateBN(file.path(DATASETS_FOLDER, dataset_per_if[currSol]), NET_MODEL)
		#AVERAGE OF RETURNS, FOR EACH CONTEXT
		# NOTO CHE DEVO FARE COSI INVECE CHE: getOverallGoalSatisf(currBN)
		# PERCHE se noi non diamo nessuna evidenza quando vogliamo sapere il  valore dei goal otterremo dei valori pesati rispetto a tutta l'esperienza passata, che normalmente non è quello che ci interessa.
		# currSol è l'IF che ha potenzialmente una configurazione diversa per ogni context config.
		ret = 0
		for(context in CONTEXTS) {
			ret = ret + exec(currBN, context, getActiveNormsPerConf(currSol, context,active_nodes_per_conf), getDisabledNormsPerConf(currSol, context, active_nodes_per_conf))
		}
		ret = ret/length(CONTEXTS)
		ret
	}
}


isBlank <- function(str) {
	grepl("^\\s*$", str)
}


getAllRelationshipsBetweenRequirements <- function(active_nodes_per_conf, reqs, bn, decompositions, direct_relationships) {
    num_conf = nrow(active_nodes_per_conf)
    rel <- list() # list of num_conf matrixes.

    #N.B. here you should search in the goal model but instead we are doing it in the bn WITH PARENTS AND CHILDREN INVERTED AS CONCEPTS
    # i.e., nodes without children are the top nodes in the goal model,
    # nodes without parents are the bottom nodes in the goal model
    # not necessary always correct TO USE THE BN, it depends on the translation between goal model and bn TODO

    top_level_req = getAllTopLevelReq(bn, reqs) #in case of only one level (no hierarchy) all req nodes are top level
    for(conf in 1:num_conf) {
        new_matrix = matrix(rep_len("", (length(reqs)*num_conf)), nrow = length(reqs), ncol = num_conf)
        rownames(new_matrix) = reqs
        rel[[conf]] = new_matrix
        for(r in top_level_req) {
            rel = writeAllRelationships(active_nodes_per_conf, conf, r, bn, rel,decompositions, direct_relationships, reqs) #recursive procedure
        }
    }

    rel
}


# NOTICE PARENTS AND CHILDREN CONCEPTS ARE INVERTED, SEE ABOVE
writeAllRelationships <- function(active_nodes_per_conf, c, r, bn, rel, decompositions, direct_relationships, reqs) {
    for(r1 in getAllParentsReq(bn, r)) { #possibly none
        rel = writeAllRelationships(active_nodes_per_conf, c, r1, bn, rel, decompositions, direct_relationships, reqs) #rec. call
    }

    curr_val = active_nodes_per_conf[c,r]
    if(curr_val == 0) { #disabled
        for(c1 in 1:num_conf) {
            if(active_nodes_per_conf[c1,r]==1) {
                rel[[c]][r,c1] = "enabled"
            } #active
            else {
                rel[[c]][r,c1] = "-"
            }
        }
    }
    else { #curr_val == 1, active
        if(length(getAllParentsReq(bn, r))==0) { #non children in the goal model (parents in the bn), i.e. it is a leaf in the GM and a root in the BN
            for(c1 in 1:num_conf) {
                if(active_nodes_per_conf[c1,r]==1) { #N.B. here one could add a test if it has been activated any additional req, e.g. to strenghten a specific req (same way as below with XOR relationships)
                    rel[[c]][r,c1] = "-"
                }
                else {
                	#case like in RE, with no direct relationships between different leaves
            		rel[[c]][r,c1] = "disabled"
                	#this added for PRIMA18, it supports now direct XOR relationships between
                	for(rn in reqs) {
                		if(direct_relationships[r,rn]=="XOR" && active_nodes_per_conf[c1,rn]==1) {
							rel[[c]][r,c1] = "alteration"
							break
	                	}
                	}
                }
            }
        }
        else {
            for(c1 in 1:num_conf) {
                if(active_nodes_per_conf[c1,r]==1) {
                    children_relationships = c() # list of relationships prev calc.
                    for(ch in getAllParentsReq(bn, r)) {
                        children_relationships = c(children_relationships, rel[[c]][ch,c1])
                    }
                    # here the type of revision/relationship
                    # depends on the relationship of children and
                    # on the type of decomposition
                    if(all(children_relationships == "-")) {
                        rel[[c]][r,c1] = "-"
                    }
					else {
						children_relationships = children_relationships[children_relationships != "-"]
						if(decompositions[r,]=="AND-decomposition") {
							if(all(children_relationships == "disabled") || all(children_relationships == "relaxed")) {
								rel[[c]][r,c1] = "relaxed"
							}
							else if(all(children_relationships == "enabled") || all(children_relationships == "strengthened")){
								rel[[c]][r,c1] = "strengthened"
							}
							else{
								rel[[c]][r,c1] = "alteration"
							}
						}
						else if(decompositions[r,]=="OR-decomposition") {
							if(all(children_relationships == "disabled") || all(children_relationships == "strengthened")){
								rel[[c]][r,c1] = "strengthened"
							}
							else if(all(children_relationships == "enabled") || all(children_relationships == "relaxed")){
								rel[[c]][r,c1] = "relaxed"
							}
							else{
								rel[[c]][r,c1] = "alteration"
							}
						}

					}
                }
                else {
                    rel[[c]][r,c1] = "disabled"
                }
            }
        }
    }
    rel
}


getAllDistances <- function(relationships_matrix, num_if, contexts) {
	dm = matrix(0, num_if, num_if)

	# foreach(i = 1:num_if) %dopar% {
	for(i in 1:num_if) {
		for(j in 1:num_if) {
			for(c in contexts) {
				dm[i,j] = dm[i,j] + getDistance(relationships_matrix, getConf(i,c), getConf(j,c))
			}
		}
	}

	dm
}


getDistance <- function(relationships_matrix, conf1, conf2) {
	dist = 0
	col = relationships_matrix[[conf1]][,conf2]
	for(r in 1:length(col)) {
		if(col[r]!="-")
			dist = dist+1
	}
	dist
}




#' isNew determines if a given if has never been tried before
#'
#' @param sol the id of the if
#' @param tried_if a list of tried if
#'
#' @return true if the if has never been tried before, false othewise
#'
#' @examples
#' isNew(1, c(1,2,3)))
isNew <- function(sol, tried_if) {
	!(sol %in% tried_if)
}


# isNew <- function(new_id, tried_if) {
# 	ret = TRUE
# 	if(new_id %in% tried_if)
# 		ret = FALSE
# 	# # if(!FAST_EXECUTION) print(tried_if)
# 	# for(tried in tried_if) {
# 	# 	# if(!FAST_EXECUTION) print(tried)
# 	# 	if(all(ALL_IF[tried,] == new))
# 	# 		ret = FALSE
# 	# }
# 	ret
# }



getIFID <- function(val_confs) {
	ret = 0
	for(i in 1:nrow(ALL_IF)) {
		if(all(ALL_IF[i,] == val_confs)) {
			ret = i
			break
		}
	}
	ret
}


getNewIF <- function(tried_if) {
	ret = c()
	for(i in 1:nrow(ALL_IF)) {
		if(!(i %in% tried_if)) {
			ret = c(ret, i)
		}
	}
	ret
}


#qui bisogna decidere a che livello: se a livello di if o di conf
getNeighborhood <- function(curr_if, all, neighborhood_version) {
	neighborhood = list()
	i=1
	sorted_neighborhood = list()
	#DEFINE THE NEIGHBORHOOD
	if(all) {
		for(r in 1:nrow(ALL_IF)) {
			if(r!=curr_if) {
				neighborhood[[i]] = ALL_IF[r,]
				i = i+1
			}
		}
		sorted_neighborhood = sortNeighborhood(neighborhood, curr_if)
	}
	else {
		#TODO COME DEFINISCO IL VICINATO DI UNA SOLUZIONE?
		#PER ORA RITORNO TUTTI ORDINA, COSì VERRA PRESO IL PIU VICINO IN TERMINI DI DISTANZA
		for(r in 1:nrow(ALL_IF)) {
			if(r!=curr_if) {
				neighborhood[[i]] = ALL_IF[r,]
				i = i+1
			}
		}
		sorted_neighborhood = sortNeighborhood(neighborhood, curr_if)
		if(neighborhood_version==1) {
			if(length(sorted_neighborhood)>neighborhood_max_length1) {
				sorted_neighborhood = sorted_neighborhood[1:neighborhood_max_length1,]
			}
		}
		if(neighborhood_version==2) {
			sorted_neighborhood = sorted_neighborhood[sorted_neighborhood[,"dist"]<=neighborhood_max_dist,]
		}
		if(neighborhood_version==3) {
			if(length(sorted_neighborhood)>neighborhood_max_length2) {
				sorted_neighborhood = sorted_neighborhood[1:neighborhood_max_length2,]
			}
		}
	}
	sorted_neighborhood
}

sortNeighborhood <- function(neighborhood,curr_if) {
	# print(neighborhood)
	neighborhood_dist = matrix(0, length(neighborhood), 2)
	colnames(neighborhood_dist) = c("index", "dist")
	neighborhood_dist[,"index"] = c(1:length(neighborhood))
	n = 1
	for(neighbor in neighborhood) {
		neighbor_id = getIFID(neighbor)
		if(neighbor_id>0)
			neighborhood_dist[n,"dist"] = distance_matrix[curr_if, neighbor_id]
		else
			neighborhood_dist[n,"dist"] = 100
		n = n+1
	}
	sorted_neighborhood = neighborhood_dist[sort.list(neighborhood_dist[,"dist"]), ]
	sorted_neighborhood
}



#' powerSetF returns all the possible combinations of active norms
#'
#' @param set a set of elements
#' 
#' @return the power set of set
#' 
#' @examples
#'  powerSetF(nodes.n)
#' 
powerSetF = function(set){ ps = vector(mode="list",length=length(set)^2); ps[[1]] = numeric(); for(e in 1:length(set)){ f = 2^(e-1); for(subset in 1:f){ ps[[f+subset]] = c(ps[[subset]],set[e]); } } ; return(ps) }

