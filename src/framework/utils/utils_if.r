


#' returns the id of the configuration currently applied in a context
#' 
#' @param context the context, a string of the form (n=="val")&&.., containing an assignment of value to ALL the contextual variables
#'
#' @return a number representing the id of the configuration
#'
#' @examples
#' getIF("paste(getStr("C_Hour", "rush"))")
#' N.B. the above example only works in case there is one and only one context variable (C_Hour) in the system
getConf <- function(if_id, context) {
	config_id = -1
	context_index = 0
	for(i in 1:length(CONTEXTS)) {
		if(CONTEXTS[i]== context) {
			context_index = i
			break
		}
	}
	if(context_index==0) {
		print(paste("context", context ,"not found"))
	}
	else {
		config_id = ALL_IF[if_id, context_index] #N.B.TODO controllare gli indici, qua potrebbe servire (solo inc erti casi, quindi dare da fuori) -1 because our indexes of configurations start from 0, but the col in the matrix start from 1
		# for(i in 1:ncol(ALL_IF)) {
		# 	if(ALL_IF[i, context_index]==1) {
		# 		config_id = i-1 
		# 		break
		# 	}
		# }
	}
	if(config_id==-1) {
		if(!FAST_EXECUTION) print(paste("no configuration active for context ", context))
	}
	config_id
}

