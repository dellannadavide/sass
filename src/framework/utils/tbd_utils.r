#' isNew determines if a given configuration has never been tried in a given context.
#'
#' @param conf the configuration, a vector of strings of names of active norms in the configuration
#' @param c the context, a string
#' 
#' @return true if the configuration has never been tried before, false othewise
#' 
#' @examples
#' isNew("N_1", paste(getStr("C_Hour", "rush")))
#' note that the above example can be executed only if in the BN there is a norm "N_1" and a context node C_Hour that can take value rush
# #'
# isNew <- function(conf, c) {
# 	conf_id = getConfId(conf)
# 	context_id = getContextId(c)
# 	TRIED_CONFS[context_id, conf_id]==0
# }
