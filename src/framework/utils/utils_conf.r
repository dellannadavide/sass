#' allConfTried determines if all possible configurations have already been tried in a given context. TODO
#'
#' @param bn a bn.fit object representing the Bayesian Network
#' @param c the context, a string
#' 
#' @return true if the all possible configurations have already been tried in c, false otherwise
#' 
#' @examples
#' allConfTried(fitted_net, paste(getStr("C_Hour", "rush")))
#' note that the above example can be executed only if in fitted_net there is a context node C_Hour that can take value rush
#' 
allConfTried <- function(c) {
	if(!SCALABILITY_EXPERIMENTATION) {
		context_id = getContextId(c)
		all(TRIED_CONFS_PER_CONTEXT[context_id,]==1)
	}
	else {
		FALSE
	}

}

updateTriedConfPerContext <- function(if_id) {
	for(i in 1:length(CONTEXTS)) {
		TRIED_CONFS_PER_CONTEXT[i,ALL_IF[if_id,i]] <<- 1
	}
}

#' getActiveNormsPerConf determines the active requirements in a given configuration
#'
#' @param conf_id the id of the configuration
#' @param active_nodes_per_conf a 0/1 matrix containing info about active norms for each configuration
#' 
#' @return a vector of strings (the labels of the nodes currently active in configuration conf_id)
#' 
#' @examples
#' getActiveNormsPerConf(1, active_nodes_per_conf)
#' 
getActiveNormsPerConf <- function(if_id, context, active_nodes_per_conf) {
	conf_id = ALL_IF[if_id,getContextId(context)]
	colnames(active_nodes_per_conf[conf_id, active_nodes_per_conf[conf_id,]==1])
}

#' getDisabledNormsPerConf determines the disabled requirements in a given configuration
#'
#' @param conf_id the id of the configuration
#' @param active_nodes_per_conf a 0/1 matrix containing info about active norms for each configuration
#' 
#' @return a vector of strings (the labels of the nodes currently active in configuration conf_id)
#' 
#' @examples
#' getDisabledNormsPerConf(1, active_nodes_per_conf)
#' 
getDisabledNormsPerConf <- function(if_id, context, active_nodes_per_conf) {
	conf_id = ALL_IF[if_id,getContextId(context)]
	colnames(active_nodes_per_conf[conf_id, active_nodes_per_conf[conf_id,]==0])
}

#' getDatasetPerConf determines the dataset for a given configuration
#'
#' @param conf_id the id of the configuration
#' @param dataset_per_conf a vector containing a list of filenames of dataset associated to each configuration
#' 
#' @return a strings, the filename of the dataset associated to configuration conf_id
#' 
#' @examples
#' getDatasetPerConf(1, dataset_per_conf)
#' 
getDatasetPerConf <- function(conf_id, dataset_per_conf) {
	dataset_per_conf[conf_id]
}

#' getNewConf determines a list of configurations never tried in a context
#'
#' @param fit a bn.fit object representing the Bayesian Network
#' @param context the context, a string
#' 
#' @return a vector of ids of conf never tried in context
#' 
#' @examples
#'  getNewConf(fit, paste(getStr("C_Hour", "rush")))
#'  notice that the aboce example only works if there is one and only of contextual variable in the BN (C_Hour)
#' 
getNewConf <- function(fit, context) {
	context_id = getContextId(context)
	r = c()
	for(i in 1:length(TRIED_CONFS_PER_CONTEXT[context_id,])) {
		if(TRIED_CONFS_PER_CONTEXT[context_id,i]==0)
			r = c(r, i)
	}
	r
}

#' getConfId determines the id of a given configuration
#'
#' @param conf the configuration, a vector of strings of names of active norms in the configuration
#'
#' @return the id of the configuration, or 0 if the configuration has not been found
#'
#' @examples
#' getConfId(c("N_1"))
#'
getConfId <- function(if_id, context, conf) {
	id = 0
	for(c in 1:nrow(active_nodes_per_conf)) {
		# nc = getActiveNormsPerConf(if_id, context, active_nodes_per_conf)
		nc = colnames(active_nodes_per_conf[c, active_nodes_per_conf[c,]==1])
		if(all(nc %in% conf) && all(conf %in% nc)) {
			id = c
			break
		}
	}
	if(id==0) {
		print(paste("WARNING: conf",conf,"not found"))
	}

	id
}


#' getActiveNormsString gives a string representing evidence for the BN for active norms given in input
#'
#' @param n_a a list of active norms
#'
#' @return a string  to give as evidence to the BN
#'
#' @examples
#' getActiveNormsString(c("N_N"))
#'
getActiveNormsString <- function(n_a) {
	n_a_s = ""
	for(i in 1:length(n_a)) {
		n_a_s = paste(n_a_s, ifelse(isBlank(n_a_s),"","&"), getStr(n_a[i], "active"),sep="")
	}
	n_a_s
}

#' getActiveNormsString gives a string representing evidence for the BN for active norms given in input, excluding the norm n
#'
#' @param n_a a list of active norms
#' @param n a norm to exlude from the list of active ones (a string with the name of the norm)
#'
#' @return a string  to give as evidence to the BN
#'
#' @examples
#' getActiveNormsString(c("N_N"), "N_N")
#'
getActiveNormsStringWithoutN <- function(n_a, n) {
	n_a_s = ""
	for(i in 1:length(n_a)) {
		n_a_s = paste(n_a_s, ifelse(n_a[i] == n, "", paste(ifelse(isBlank(n_a_s),"","&"), getStr(n_a[i], "active"))),sep="")
	}
	n_a_s
}

#' getDisabledNormsString gives a string representing evidence for the BN for disabled norms given in input
#'
#' @param n_a a list of disabled norms
#'
#' @return a string  to give as evidence to the BN
#'
#' @examples
#' getDisabledNormsString(c("N_N"))
#'
getDisabledNormsString <- function(n_d) {
	n_d_s = ""
	for(i in 1:length(n_d)) {
		n_d_s = paste(n_d_s, ifelse(isBlank(n_d_s),"","&"), getStr(n_d[i], "disabled"),sep="")
	}
	n_d_s
}

#' getConfString gives a string representing the input to give to a bn as evidence, including the active norms the disabled ones and the context
#'
#' @param n_a a list of active norms
#' @param n_d a list of disabled norms
#' @param c a string for the context
#'
#' @return a string  to give as evidence to the BN
#'
#' @examples
#' getConfString(c("N_J"), c("N_N"), paste(getStr("C_Hour", "rush")))
#'
getConfString <- function(n_a, n_d, c) {
	n_a_s = ""
	n_d_s = ""
	n_a_s = getActiveNormsString(n_a)
	if(length(n_d)>0){
		n_d_s = getDisabledNormsString(n_d)
	}
	paste(n_a_s, ifelse(isBlank(n_a_s),"","&"), n_d_s, ifelse(isBlank(n_d_s),"","&"), c,sep="")
}


#' getConfString gives a string representing the input to give to a bn as evidence, including the active norms (excluding one norm), the disabled ones and the context
#'
#' @param n_a a list of active norms
#' @param n a norm to exlude from the list of active ones (a string with the name of the norm)
#' @param n_d a list of disabled norms
#' @param c a string for the context
#'
#' @return a string  to give as evidence to the BN
#'
#' @examples
#' getConfString(c("N_J"), "N_J", c("N_N"), paste(getStr("C_Hour", "rush")))
#'
getConfStringWithoutN <- function(n_a, n, n_d, c) {
	n_a_s = ""
	n_d_s = ""
	n_a_s = getActiveNormsStringWithoutN(n_a, n)
	if(length(n_d)>0){
		n_d_s = getDisabledNormsString(n_d)
	}
	paste(n_a_s, ifelse(isBlank(n_a_s),"","&"), n_d_s, ifelse(isBlank(n_d_s),"","&"), c,sep="")
}