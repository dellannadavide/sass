library(bnlearn)
set.seed(RANDOM_SEED)
require(plotrix)

source(file.path("utils", "utils.r"))
source(file.path("baseHillClimbing", "baseHillClimbing.r"))
source(file.path("BNstateBased", "BNstateBased.r"))
source(file.path("BNstateBased", "BNstateBasedEXP.r"))
source(file.path("BNstateBased", "BNstateBasedEXP_pure.r"))
source(file.path("hillClimbingMod", "hillClimbingMod.r"))
source(file.path("pureHillClimbing", "pureHillClimbing.r"))
source(file.path("pureHillClimbingMod", "pureHillClimbingMod.r"))

# Setting the working directory to the simulation folder
setwd(SIMULATION_FOLDER)

# A matrix representing the set of active requirements for each possible variant
active_nodes_per_conf = read.table(VARIANTS_TABLE, header = TRUE, sep = "\t")

# The number of variants
num_conf = nrow(active_nodes_per_conf)

# A vector containing all system configurations
ALL_IF <-  read.table(CONFIGURATIONS_TABLE, header = TRUE, sep = "\t")
# A vector containing all the possible system configurations, given the variants (not necessarily applicable)
# ALL_IF <-  expand.grid(rep(list(1:num_conf),length(contexts)))

# The number of system configurations
num_if = nrow(ALL_IF)

# A dataset containing the datasets for all system configurations
dataset_per_if <- c()
for(i in 1:num_if) {
	dataset_per_if <- c(dataset_per_if, paste0(DATASET_NAME_TYPE, i, ".txt"))
}

# A list of matrices, each of them describes the relationships between the same requirement in all the requirement variants
# e.g. The following matrix describes the revision of all requirements in all variants, compared to variant 1
# 	"X1"	"X2"	"X3"	"X4"	"X5"	"X6"	"X7"	"X8"	"X9"	"X10"	"X11"	"X12"
# "N_N"	"-"	"alteration"	"disabled"	"disabled"	"disabled"	"-"	"alteration"	"-"	"alteration"	"disabled"	"-"	"alteration"
# "N_NP"	"-"	"alteration"	"disabled"	"disabled"	"disabled"	"-"	"alteration"	"-"	"alteration"	"disabled"	"-"	"alteration"
# "N_NPS"	"-"	"disabled"	"disabled"	"disabled"	"disabled"	"-"	"disabled"	"-"	"disabled"	"disabled"	"-"	"disabled"
# "N_NPA"	"-"	"enabled"	"-"	"-"	"-"	"-"	"enabled"	"-"	"enabled"	"-"	"-"	"enabled"
# "N_NN"	"-"	"-"	"disabled"	"disabled"	"disabled"	"-"	"-"	"-"	"-"	"disabled"	"-"	"-"
# "N_J"	"-"	"-"	"-"	"alteration"	"relaxed"	"relaxed"	"relaxed"	"alteration"	"alteration"	"alteration"	"alteration"	"alteration"
# "N_JT"	"-"	"-"	"-"	"alteration"	"disabled"	"disabled"	"disabled"	"alteration"	"alteration"	"alteration"	"alteration"	"alteration"
# "N_JTS"	"-"	"-"	"-"	"disabled"	"disabled"	"disabled"	"disabled"	"disabled"	"disabled"	"disabled"	"disabled"	"disabled"
# "N_JTA"	"-"	"-"	"-"	"-"	"-"	"-"	"-"	"enabled"	"enabled"	"enabled"	"-"	"-"
# "N_JTP"	"-"	"-"	"-"	"enabled"	"-"	"-"	"-"	"-"	"-"	"-"	"enabled"	"enabled"
# "N_JN"	"-"	"-"	"-"	"-"	"-"	"-"	"-"	"-"	"-"	"-"	"-"	"-"
relationships_matrix <- getAllRelationshipsBetweenRequirements(active_nodes_per_conf, reqs, generateBN(file.path(DATASETS_FOLDER, dataset_per_if[1]), NET_MODEL), decompositions, direct_relationships)
# for(i in 1:length(relationships_matrix)) {
# 	write.table(relationships_matrix[i], paste("RELATIONSHIPS_", i, ".csv", sep="") , sep = "\t", row.names = TRUE, col.names = TRUE)
# }


# A matrix of distances between all possible system configurations, in terms of number of revisions performed to move from one to the other w.r.t. the relationships_matrix
distance_matrix <- getAllDistances(relationships_matrix, num_if, CONTEXTS)
#write.table(distance_matrix, "DIST.csv", sep = "\t", row.names = TRUE)

# A matrix containing info about variants tried in each context
# rows are all the possible operating context
# columns are all the possible requirement variants
# val 0 in a cell (i,j) means variant (j) not tried in the context i, val 1 means tried.
TRIED_CONFS_PER_CONTEXT <- matrix(0, nrow=length(CONTEXTS), ncol=num_conf)

# A vector containing info about all the system configurations tried so far
TRIED_IF <- c()

# Variable which will contain the current dataset, dynamically generated step by step during the SASS optimization process
currDataset <- ""


# The returns matrix defined in the config file
returnsMatrix <- read.table(RETURNS_MATRIX, header = TRUE, sep = "\t")

# A vector contianing the initial info known by SASS about the average objectives achievement probability
init_ret_known <- rep(0.5, num_if)
ret_known <- init_ret_known


if(!FAST_EXECUTION) {
	print("WARNING! REMEMBER TO CALCULATE ALL NEEDED VALUES IF YOU STILL DID NOT.")
}
#TO UNCOMMENT ALL THE FOLLOWING TO CALCULATE ALL THE REQUIRED VALUES
#RETURN
#print("calc all returns...")
#returns <- calculateAllReturns(num_if,dataset_per_if)
#print(returns)

#RETURNS_MATRIX
#CALCULATE RETURN MATRIXRE18
# returnsMatrix <- matrix(calculateMatrixReturns(num_if,dataset_per_if), nrow=num_if, ncol=29, byrow = TRUE)
# colnames(returnsMatrix)  <- c("GLOBAL", "C1_GLOBAL", "C1O_1", "C1O_2", "C1O_N1", "C1O_N2", "C1O_N3", "C1_PROB", "C2_GLOBAL", "C2O_1", "C2O_2", "C2O_N1", "C2O_N2", "C2O_N3", "C2_PROB", "C3_GLOBAL", "C3O_1", "C3O_2", "C3O_N1", "C3O_N2", "C3O_N3", "C3_PROB", "C4_GLOBAL", "C4O_1", "C4O_2", "C4O_N1", "C4O_N2", "C4O_N3", "C4_PROB")

#CALCULATE RETURN MATRIX PRIMA18
#returnsMatrix <- matrix(calculateMatrixReturns(num_if,dataset_per_if), nrow=num_if, ncol=17, byrow = TRUE)
#colnames(returnsMatrix)  <- c("GLOBAL", "C1_GLOBAL", "C1O_1", "C1O_2", "C1_PROB", "C2_GLOBAL", "C2O_1", "C2O_2", "C2_PROB", "C3_GLOBAL", "C3O_1", "C3O_2", "C3_PROB", "C4_GLOBAL", "C4O_1", "C4O_2", "C4_PROB")
#write.table(returnsMatrix, "returnsMatrixPRIMA.csv", sep = "\t", row.names = FALSE)