

# Seed for random numbers generation
RANDOM_SEED <- 1165

# The number of random observations to generate from a network when asking queries
INFERENCE_PARAM <- 700000

# The folder containing the material concerning the simulations.
# N.B. This folder will also be the WORKING DIRECTORY for SASS
SIMULATION_FOLDER <- file.path(SASS_FOLDER, "simulations", "crowdnavrtx")

# The folder containing the datasets to use to train the network during the execution of SASS_FOLDER
# Notice that all datasets should be named according to the following rule: "<DATASET_NAME_TYPE>_<n>" where DATASET_NAME_TYPE is below defined and n is an integer s.t. 1 <= n <= number of system configurations
DATASETS_FOLDER <-  file.path(SIMULATION_FOLDER, "dataset", "IF")
DATASET_NAME_TYPE <- "IF"

# The model of the Bayesian Network
NET_MODEL <- "[C_W][C_T][V_NPAP][N_NN|C_W:C_T][N_NPS|C_W:C_T][N_NPA|C_W:C_T:V_NPAP][N_NP|N_NPA:V_NPAP:N_NPS][N_N|N_NP:N_NN:C_W:C_T][O_N1|N_NPA:V_NPAP:N_NPS:C_W:C_T][O_N2|N_NPA:V_NPAP:N_NPS:C_W:C_T][O_N3|N_NPA:V_NPAP:N_NPS:C_W:C_T][N_JTA|C_W:C_T][N_JN|C_W:C_T][N_JTS|C_W:C_T][N_JTP|C_W:C_T][N_JT|N_JTA:N_JTS:N_JTP][N_J|N_JT:N_JN][O_1|N_N:N_J:C_W:C_T][O_2|N_J:C_W:C_T]"
#NET_MODEL PRIMA18
#NET_MODEL <- "[C_W][C_T][N_NPS|C_W:C_T][N_NPA|C_W:C_T][N_JTA|C_W:C_T][N_JTS|C_W:C_T][N_JTP|C_W:C_T][O_1|N_NPS:N_NPA:N_JTA:N_JTS:N_JTP:C_W:C_T][O_2|N_NPS:N_NPA:N_JTA:N_JTS:N_JTP:C_W:C_T]"

# The list of requirement nodes in NET_MODEL
reqs <- c("N_N", "N_NP", "N_NPS", "N_NPA", "N_NN", "N_J", "N_JT", "N_JTS", "N_JTA", "N_JTP", "N_JN")
#reqs PRIMA18
#REQS <- c("N_NPS", "N_NPA", "N_JTS", "N_JTA", "N_JTP")

# A matrix describing the requirement refinements in the model
# e.g., the instantiation below reported creates a matrix
# 		decomposition
# N_N	"AND-decomposition"
# N_NP	"OR-decomposition"
# N_J	"AND-decomposition"
# N_JT	"OR-decomposition"
# such matrix states that the requirements N_N and N_J are AND-refined, while requirements N_NP and N_JT are OR-defined
decompositions <- matrix(c("AND-decomposition","OR-decomposition","AND-decomposition","OR-decomposition"),nrow=4,ncol=1,byrow = TRUE)
rownames(decompositions) <- c("N_N","N_NP", "N_J", "N_JT")
colnames(decompositions) <- c("decomposition")


# A matrix describing direct relationships between requirements
# e.g., the instantiation below reported, if decommented, creates a matrix
# N_N N_NP N_NPS N_NPA N_NN N_J N_JT N_JTS N_JTA N_JTP N_JN
# N_N   "-" "-"  "-"   "-"   "-"  "-" "-"  "-"   "-"   "-"   "-"
# N_NP  "-" "-"  "-"   "-"   "-"  "-" "-"  "-"   "-"   "-"   "-"
# N_NPS "-" "-"  "-"   "XOR" "-"  "-" "-"  "-"   "-"   "-"   "-"
# N_NPA "-" "-"  "XOR" "-"   "-"  "-" "-"  "-"   "-"   "-"   "-"
# N_NN  "-" "-"  "-"   "-"   "-"  "-" "-"  "-"   "-"   "-"   "-"
# N_J   "-" "-"  "-"   "-"   "-"  "-" "-"  "-"   "-"   "-"   "-"
# N_JT  "-" "-"  "-"   "-"   "-"  "-" "-"  "-"   "-"   "-"   "-"
# N_JTS "-" "-"  "-"   "-"   "-"  "-" "-"  "-"   "XOR" "XOR" "-"
# N_JTA "-" "-"  "-"   "-"   "-"  "-" "-"  "XOR" "-"   "XOR" "-"
# N_JTP "-" "-"  "-"   "-"   "-"  "-" "-"  "XOR" "XOR" "-"   "-"
# N_JN  "-" "-"  "-"   "-"   "-"  "-" "-"  "-"   "-"   "-"   "-"
# such matrix described direct relationships between all nodes, and in this particular cases, only XOR relationships between some of them
# N.B. to specify such relationships only in case of no hierarchy between requirements expressed in the network
direct_relationships <- matrix(rep_len("-", (length(reqs)*length(reqs))), nrow = length(reqs), ncol=length(reqs))
rownames(direct_relationships) <- reqs
colnames(direct_relationships) <- reqs
# direct_relationships["N_NPS", "N_NPA"] = "XOR"
# direct_relationships["N_NPA", "N_NPS"] = "XOR"
# direct_relationships["N_JTS", "N_JTA"] = "XOR"
# direct_relationships["N_JTA", "N_JTS"] = "XOR"
# direct_relationships["N_JTS", "N_JTP"] = "XOR"
# direct_relationships["N_JTP", "N_JTS"] = "XOR"
# direct_relationships["N_JTA", "N_JTP"] = "XOR"
# direct_relationships["N_JTP", "N_JTA"] = "XOR"

# A csv file containing a table with all the requirement variants considered
# e.g., an example of such table is
# N_N	N_NP	N_NPS	N_NPA	N_NN	N_J	N_JT	N_JTS	N_JTA	N_JTP	N_JN
# 1	1	1	0	1	1	1	1	0	0	1
# 1	1	0	1	1	1	1	1	0	0	1
# 0	0	0	0	0	1	1	1	0	0	1
# 0	0	0	0	0	1	1	0	0	1	1
# 0	0	0	0	0	1	0	0	0	0	1
# 1	1	1	0	1	1	0	0	0	0	1
# 1	1	0	1	1	1	0	0	0	0	1
# 1	1	1	0	1	1	1	0	1	0	1
# 1	1	0	1	1	1	1	0	1	0	1
# 0	0	0	0	0	1	1	0	1	0	1
# 1	1	1	0	1	1	1	0	0	1	1
# 1	1	0	1	1	1	1	0	0	1	1
VARIANTS_TABLE <- file.path(SIMULATION_FOLDER, "variants.csv")

# A csv file containing a table with all the system configurations tried
CONFIGURATIONS_TABLE <- file.path(SIMULATION_FOLDER, "configurations.csv")

# A vector containing strings describing all possible operating contexts, expressed with a syntax that allows to use such strings as evidence in the BN
CONTEXTS <- c(
"(C_T==\"night\")&(C_W==\"normal\")",
"(C_T==\"day\")&(C_W==\"normal\")",
"(C_T==\"night\")&(C_W==\"extreme\")",
"(C_T==\"day\")&(C_W==\"extreme\")")


# A vector containing the average objectives achievement probability with all the possible system configurations
RETURNS <- c(0.0, 0.0456216577540107, 0.0876221418629213, 0.0, 0.0439814814814815, 0.0874649565461172,
0.0, 0.0418011620400258, 0.0874863453027842, 0.0, 0.0413223140495868, 0.088381414186713,
0.0, 0.0408560311284047, 0.0877675017314594, 0.0, 0.0417737789203085, 0.0878162949259672,
0.0476860559538599, 0.0922693880817768, 0.134875894144684, 0.047297157050639, 0.089377665559721, 0.133981825792103,
0.047629329859926, 0.0876463789008753, 0.135622793530071, 0.0, 0.0470144131777625, 0.0878556679382634,
0.0, 0.0415879017013233, 0.0876625640668038, 0.0, 0.0419660352710647, 0.0880106219115259, 0.0,
0.0460875331564987, 0.0882417921544148, 0.0, 0.0485372340425532, 0.0870683475987688, 0.0,
0.0416393442622951, 0.0879677872813612, 0.0473451543571708, 0.0936663123521224,
0.13502265440575, 0.0476144905345212, 0.0885865785778372, 0.135689918107314,
0.0469439062163607, 0.0912055026446461, 0.135915543602752, 0.187261146496815,
0.235333872748256, 0.275091873676166, 0.193154402895054, 0.230920770460483, 0.27539734527369,
0.192501575299307, 0.234843975468975, 0.279034770975629, 0.193315858453473, 0.228191975893177,
0.278511307610169, 0.184542102028273, 0.227753873495868, 0.278391575620404, 0.1890664160401,
0.230137010525399, 0.27915514460437, 0.240262522791993, 0.279554041849056, 0.326899289582013,
0.238949679373726, 0.278272520923633, 0.326268448127204, 0.239238013051926, 0.283135490531108,
0.328657877876633, 0.494725356892153, 0.452211672780098, 0.357134627939775)

#RETURNS PRIMA
#RETURNS <-c(0.236618243478371, 0.31450426750799, 0.366218066130281, 0.236757796487245, 0.314453774297805,
#0.366790074262334, 0.236809327915925, 0.315667861643942, 0.366995148563978, 0.236740707880478,
#0.315997100504544, 0.366931268681816, 0.236703086173865, 0.315102501718801, 0.366863225315604,
#0.236516975328257, 0.315538579604605, 0.366583247387259, 0.304568463889785, 0.384313965050393,
#0.434966628074083, 0.304792039172765, 0.381390171597697, 0.434999995699499, 0.304645662669656,
#0.384522459616224, 0.434353359514403, 0, 0.0785422216715197, 0.129847413236838, 0,
#0.077798315253621, 0.129916175948018, 0, 0.0775418474891506, 0.130489971672821, 0,
#0.0781670746634027, 0.13015764612145, 0, 0.0785232843137255, 0.129668069547333, 0,
#0.0791590493601463, 0.129838719922758, 0.0677270989855191, 0.146343002336135, 0.198004925062281,
#0.0679059869182862, 0.147537089561102, 0.198368075039757, 0.0680887692500313, 0.1466220887058,
#0.198218852562603, 0.223958749016612, 0.302699335498068, 0.354595800527988, 0.223587300325385,
#0.299941524118356, 0.353858085505688, 0.223808642678205, 0.301999049184813, 0.352982280610326,
#0.22376405777245, 0.302734655641804, 0.35402115394712, 0.223643132630332, 0.302409970253886,
#0.354200445210808, 0.223964626889492, 0.302930376161338, 0.354225831125175, 0.291553772539646,
#0.37037132626877, 0.422071417589507, 0.291336076246837, 0.370902231036823, 0.422008245698628,
#0.292363268158493, 0.37155528866929, 0.422103293873764, 0.53955927530862, 0.534785083605917, 0.394400310880863)

# A matrix containing precalculated values for several probabilities in all system configurations, to speed up the execution
# e.g. a partial matrix could be
# "GLOBAL"	"C1_GLOBAL"	"C1O_1"	"C1O_2"	"C1O_N1"	"C1O_N2"	"C1O_N3"	"C1_PROB"	"C2_GLOBAL"	"C2O_1"	"C2O_2"	"C2O_N1"	"C2O_N2"	"C2O_N3"	"C2_PROB"	"C3_GLOBAL"	"C3O_1"	"C3O_2"	"C3O_N1"	"C3O_N2"	"C3O_N3"	"C3_PROB"	"C4_GLOBAL"	"C4O_1"	"C4O_2"	"C4O_N1"	"C4O_N2"	"C4O_N3"	"C4_PROB"
# 0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0
# 0.0456216577540107	0	0	1	0	0	0	0.25	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0.182486631016043	0.826194909993793	0.374031007751938	0.724535554131967	1	0.764364105874758	0.833376029257832
# 0.0876221418629213	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0.350488567451685	0.828807308691644	0.626114106898382	0.841473049789195	1	0.796317670842864	1
# ...
RETURNS_MATRIX <- file.path(SIMULATION_FOLDER, "returnsMatrix.csv")
#"returnsMatrixPRIMA.csv"

# Parameter for the execution of SASS
# when TRUE, info contained in matrixes such as RETURNS_MATRIX or RETURNS are considered
# when FALSE, such info are calculated when needed
# N.B. It is suggested to run the script once to calculate such values, then store them and run with FAST_EXECUTION the entire iterative approach
FAST_EXECUTION = TRUE

#Parameter for the execution of scalability experimentations
#still TODO
SCALABILITY_EXPERIMENTATION = FALSE